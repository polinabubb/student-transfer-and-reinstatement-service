all: build down up

up:
	docker-compose up -d

down:
	docker-compose down

build:
	docker-compose build

test:
	docker-compose run backend poetry run pytest --cov

lint:
	docker-compose run --no-deps backend poetry run ruff src

migrate:
	docker-compose run backend alembic upgrade head
