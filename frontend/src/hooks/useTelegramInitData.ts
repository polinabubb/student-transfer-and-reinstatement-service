import {useEffect, useMemo, useState} from 'react';
import {TelegramWebApps} from 'telegram-webapps-types';
import {TelegramUser, WebApp} from '../types/types';

/**
 * Hook to get the initial data from the Telegram Web Apps API already parsed.
 * @example
 * const { hash } = useTelegramInitData();
 * console.log({ hash });
 */

export type TelegramContext = {
    webApp?: WebApp;
    user?: TelegramUser;
}

function useTelegramInitData() {

    const [webApp, setWebApp] = useState<TelegramWebApps.WebApp | null>(null);

    useEffect(() => {
        const app = (window as any).Telegram?.WebApp;
        if (app) {
            app.ready();
            setWebApp(app);
        }
    }, []);

    return useMemo(() => {
        return webApp
            ? {
                webApp,
                unsafeData: webApp.initDataUnsafe,
                user: webApp.initDataUnsafe.user,
            }
            : {};
    }, [webApp]);
}

export default useTelegramInitData;