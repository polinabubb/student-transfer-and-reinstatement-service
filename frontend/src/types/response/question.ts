export type QuestionType =
{
    id: number,
    question:string,
    answer: string,
    is_frequent: boolean,
    count: number,
    tags: string[]
}
