export type FacultiesResponse =
    {
        id: number,
        code: string,
        name: string,
        level: string,
        form: string,
        link: string,
        educational_program: string,
        price: number,
        updated_at: string,
        created_at: string,
        short_name: string,
        courses: Course[]
    }

export type Course = {
    id: number,
    number: number,
    number_budget: number,
    number_budget_subject: number,
    number_budget_local: number,
    number_tuition: number,
    updated_at: string,
    created_at: string,
    faculty_code: string,
}
