export type Login =
    {
        id: number | undefined,
        is_bot: boolean,
        first_name: string | undefined,
        last_name: string | undefined,
        username: string | undefined,
        language_code: string | undefined,
        is_premium: boolean,
        added_to_attachment_menu: boolean,
        allows_write_to_pm: boolean,
        photo_url: string
    }
