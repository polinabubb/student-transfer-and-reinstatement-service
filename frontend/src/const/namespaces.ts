export enum NameSpace {
    Faculties = 'FACULTIES',
    User = 'USER',
    Questions = 'QUESTIONS'
}
