export const API_PATHS = {
    Faculties: () => '/faculties?skip=0&limit=100',
    Login: () =>'/telegram/auth/login',
    Question: () =>'/questions?skip=0&limit=100&is_frequent=true'
}
