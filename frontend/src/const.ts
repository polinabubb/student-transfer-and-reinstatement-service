import {NavigationSection} from '@components/header/types';

export enum AppRoute {
    Main = '/',
    Checklist = '/checklist',
    Questions = '/questions',
}

export enum NavigationTitle {
    Directions = 'Направления',
    Checklist = 'Чеклист',
    Questions = 'Вопросы'
}

export enum CssTitle {
    Directions = 'directions',
    Checklist = 'checklist',
    Questions = 'questions'
}

export const NavigationSections: NavigationSection[] =
    [
        {
            title: NavigationTitle.Directions,
            path: AppRoute.Main,
            cssTitle: CssTitle.Directions,
        },
        {
            title: NavigationTitle.Checklist,
            path: AppRoute.Checklist,
            cssTitle: CssTitle.Checklist,
        },
        {
            title: NavigationTitle.Questions,
            path: AppRoute.Questions,
            cssTitle: CssTitle.Questions,
        }
    ]

export enum FilterTitle {
    coast = 'цена',
    place = 'места',
    course = 'курс',
}

export enum SortingTitle {
    coast = 'по цене',
    freePlace = 'по бюджетным местам',
    paidPlaces = 'по платным местам',
    allPlace = 'по всем местам',
}

export enum Theme {
    dark = 'dark',
    light = 'light',
}

