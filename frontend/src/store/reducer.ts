import {combineReducers} from "redux";
import {NameSpace} from "../const/namespaces";
import {FacultiesProcess} from "./faculties/process";
import {LoginProcess} from "./user/process";
import {QuestionProcess} from "./questions/process";


export const reducer = combineReducers({[NameSpace.Faculties]: FacultiesProcess.reducer,
    [NameSpace.User]: LoginProcess.reducer,
    [NameSpace.Questions]: QuestionProcess.reducer})
