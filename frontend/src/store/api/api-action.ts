import {createAsyncThunk} from "@reduxjs/toolkit";
import {FacultiesResponse} from "../../types/response/facuities-response";
import {AppDispatch, State} from "../use-app/use-app";
import {AxiosInstance} from "axios";
import {API_PATHS} from "../../const/paths";
import {Login} from "../../types/request/login";
import {Token} from "../../types/response/token";
import {QuestionType} from "../../types/response/question";

export const fetchAllFaculties = createAsyncThunk<FacultiesResponse[],undefined,
    {
        dispatch: AppDispatch;
        state: State;
        extra: AxiosInstance;
    }
>('api/fetchFaculties', async (_arg,{extra: api}) => await api.get<FacultiesResponse[]>(API_PATHS.Faculties()).then((res)=>res.data));

export const fetchQuestions = createAsyncThunk<QuestionType[],undefined,
    {
        dispatch: AppDispatch;
        state: State;
        extra: AxiosInstance;
    }
>('api/questions', async (_arg,{extra: api}) => await api.get<QuestionType[]>(API_PATHS.Question()).then((res)=>res.data));

export const telegramLogin = createAsyncThunk<Token,Login,
    {
        dispatch: AppDispatch;
        state: State;
        extra: AxiosInstance;
    }
>('api/login', async (login:Login,{extra: api}) => {
    return await api.post<Token>(API_PATHS.Login(), login).then((res) => res.data);
});
