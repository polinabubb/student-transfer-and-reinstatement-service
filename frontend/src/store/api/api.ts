import axios, {AxiosInstance, InternalAxiosRequestConfig} from 'axios';

const BASE_URL ='https://vobladogs.ru/api/v1'
const REQUEST_TIMEOUT = 5000; // Замените это на ваше время ожидания

export const createAPI = (): AxiosInstance => {
    const api = axios.create({
        baseURL: BASE_URL,
        timeout: REQUEST_TIMEOUT,
    });
    api.interceptors.request.use(
        (config: InternalAxiosRequestConfig) => {

            return config;
        }
    );
    return api;
};


