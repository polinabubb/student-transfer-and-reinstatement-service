import {createSlice} from '@reduxjs/toolkit'
import {NameSpace} from "../../const/namespaces";
import {fetchQuestions} from "../api/api-action";
import {Question} from "@components/question/types";

function getParagraphs(text: string): string[] {
    let splited = text.split('. ');
    let outArray: string[] = [];
    let current: string = '';

    for (let i = 0; i < splited.length; i++) {
        let isLast = i === splited.length - 1;
        let dot = isLast ? '' : '. ';
        current += splited[i].trim() + dot;
        if (current.length >= 100) {
            outArray.push(current.trim());
            current = '';
        }
    }

    if (current !== '') {
        outArray.push(current);
    }

    return outArray;
}

export type questionState = {
    questions: Question[];
}

const initialState: questionState = {
    questions: [],
};

export const QuestionProcess = createSlice({
        name: NameSpace.Questions,
        initialState: initialState,
        reducers: {},
        extraReducers: function (builder) {
            builder.addCase(fetchQuestions.fulfilled, (state, action) => {
                const question = action.payload;

                state.questions = question.map((q) => {
                    return {
                        id: q.id,
                        question: q.question,
                        answer: getParagraphs(q.answer)
                    }
                })
            })
        }
    }
)

