import {State} from "../use-app/use-app";
import {NameSpace} from "../../const/namespaces";
import {Question} from "@components/question/types";

export const getQuestions = (state: Pick<State, NameSpace.Questions>): Question[] => state[NameSpace.Questions].questions;
