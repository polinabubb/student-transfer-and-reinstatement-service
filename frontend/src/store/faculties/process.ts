import {createSlice} from '@reduxjs/toolkit'
import {NameSpace} from "../../const/namespaces";
import {fetchAllFaculties} from "../api/api-action";
import {Direction} from "@components/direction-cards/types";
import {FilterTitle, SortingTitle} from "../../const";


export const saveCourse = (course: number) => localStorage.setItem('course', course.toString())
export const getCourse = (): string => localStorage.getItem('course') ?? '1';
export const savePrice = (price: number) => localStorage.setItem('price', price.toString())
export const getPrice = (): string | null => localStorage.getItem('price');
export const saveBudget = (budget: boolean) => localStorage.setItem('budget', budget.toString())
export const getBudget = (): boolean => localStorage.getItem('budget') === "true";
export const savePaid = (paid: boolean) => localStorage.setItem('paid', paid.toString())
export const getPaid = (): boolean => localStorage.getItem('paid') === "true";
export const saveSorting = (sort: string | null) => localStorage.setItem('sort', sort ?? '')
export const getSorting = (): SortingTitle | null => localStorage.getItem('sort') !== '' ? <SortingTitle>localStorage.getItem('sort') : null;


export function checkPlace(budgetPlaces: number, paidPlaces: number, budget: boolean, paid: boolean): boolean {
    if (budget && budgetPlaces === 0)
        return false;
    return !(paid && paidPlaces === 0);
}

export function sort(directions: Direction[], sortFilter: SortingTitle | null): Direction[] {
    if (sortFilter == null)
        return directions;
    if (sortFilter === SortingTitle.paidPlaces)
        return directions.sort((a, b) => a.paidPlaces >= b.paidPlaces ? 1 : -1)
    if (sortFilter === SortingTitle.freePlace)
        return directions.sort((a, b) => a.budgetPlaces >= b.budgetPlaces ? 1 : -1)
    if (sortFilter === SortingTitle.allPlace)
        return directions.sort((a, b) => a.paidPlaces + a.budgetPlaces >= b.paidPlaces + b.budgetPlaces ? 1 : -1)
    else {
        return directions.sort((a, b) => a.price >= b.price ? 1 : -1)
    }
}

export type Faculties = {
    loading: boolean;
    allDirections: Direction[],
    filteredDirections: Direction[],
    sortedFilter: SortingTitle | null,
    course: number,
    maxPrice: number,
    minPrice: number,
    priceValue: number,
    currentValue: number,
    activeFilter: FilterTitle | null | 'Sorting',
    budget: boolean,
    paid: boolean,
}

const initialSate: Faculties = {
    loading: true,
    allDirections: [],
    filteredDirections: [],
    sortedFilter: null,
    course: 1,
    maxPrice: 0,
    minPrice: 0,
    priceValue: 0,
    currentValue: 0,
    activeFilter: null,
    budget: false,
    paid: false
};

export const FacultiesProcess = createSlice({
        name: NameSpace.Faculties,
        initialState: initialSate,
        reducers: {
            setCourse(state, action: { payload: number }) {
                state.course = action.payload;
                saveCourse(state.course);
                state.maxPrice = state.allDirections.filter((direction) => direction.course === state.course).reduce((max, direction) => Math.max(max, direction.price), 0);
                state.currentValue = state.maxPrice;
                state.minPrice = state.filteredDirections.reduce((min, direction) => Math.min(min, direction.price), 100000000);
                state.priceValue = state.currentValue;
                const dir = state.allDirections.filter((direction) => direction.course === state.course
                    && direction.price <= state.priceValue && checkPlace(direction.budgetPlaces, direction.paidPlaces, state.budget, state.paid));
                state.filteredDirections = sort(dir, state.sortedFilter);
            },
            setPriceValue(state, action: { payload: number }) {
                state.currentValue = action.payload;
            },
            setActiveFilter(state, action: { payload: FilterTitle | null | 'Sorting' }) {
                state.activeFilter = action.payload;
                state.currentValue = Math.min(state.priceValue, state.maxPrice);
            },
            doPriceFilter(state) {
                state.priceValue = state.currentValue;
                savePrice(state.priceValue);
                const dir = state.allDirections.filter((direction) => direction.course === state.course
                    && direction.price <= state.priceValue && checkPlace(direction.budgetPlaces, direction.paidPlaces, state.budget, state.paid));
                state.filteredDirections = sort(dir, state.sortedFilter);
            },
            setBudgetPlace(state, action: { payload: boolean }) {
                state.budget = action.payload;
            },
            setPaidPlace(state, action: { payload: boolean }) {
                state.paid = action.payload;
            },
            doPlaceFilter(state) {
                saveBudget(state.budget);
                savePaid(state.paid);
                state.activeFilter = null;
                const dir = state.allDirections.filter((direction) => direction.course === state.course
                    && direction.price <= state.priceValue && checkPlace(direction.budgetPlaces, direction.paidPlaces, state.budget, state.paid));
                state.filteredDirections = sort(dir, state.sortedFilter);
            },
            doSort(state, action: { payload: SortingTitle }) {
                state.activeFilter = null;
                if (state.sortedFilter === action.payload) {
                    state.sortedFilter = null;
                } else {
                    state.sortedFilter = action.payload;
                }
                saveSorting(state.sortedFilter);
                state.filteredDirections = sort(state.filteredDirections, state.sortedFilter)
            },

        },
        extraReducers: function (builder) {
            builder.addCase(fetchAllFaculties.pending, (state) => {
                state.loading = true;
            })
            builder.addCase(fetchAllFaculties.fulfilled, (state, action) => {
                state.loading = false;
                const response = action.payload;
                const faculties = response.map(res => res.courses.map(course => {
                    const dir: Direction =
                        {
                            id: course.id,
                            abbreviation: res.short_name,
                            abbreviationDecoding: res.name,
                            budgetPlaces: course.number_budget,
                            paidPlaces: course.number_tuition,
                            price: res.price,
                            link: res.link,
                            course: course.number
                        };
                    return dir;
                }))
                state.course = parseInt(getCourse());
                state.budget = getBudget();
                state.paid = getPaid();
                state.sortedFilter = getSorting();
                state.allDirections = faculties.flat();
                const dir = state.allDirections.filter((direction) => direction.course === state.course
                    && checkPlace(direction.budgetPlaces, direction.paidPlaces, state.budget, state.paid));
                state.filteredDirections = sort(dir, state.sortedFilter);
                state.maxPrice = state.filteredDirections.reduce((max, direction) => Math.max(max, direction.price), 0);
                state.currentValue = state.maxPrice;
                state.minPrice = state.filteredDirections.reduce((min, direction) => Math.min(min, direction.price), 100000000);
                const price = getPrice();
                state.priceValue = price ? parseInt(price) : state.maxPrice;
            })
        }
    }
)

export const {
    setCourse,
    setPriceValue,
    setActiveFilter,
    doPriceFilter,
    setBudgetPlace,
    setPaidPlace,
    doPlaceFilter,
    doSort
} = FacultiesProcess.actions;
