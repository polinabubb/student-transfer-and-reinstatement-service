import {NameSpace} from "../../const/namespaces";
import {State} from "../use-app/use-app";
import {Direction} from "@components/direction-cards/types";
import {FilterTitle, SortingTitle} from "../../const";

export const getAllFaculties = (state: Pick<State, NameSpace.Faculties>): Direction[] => state[NameSpace.Faculties].allDirections;
export const getFilteredFilms = (state: Pick<State, NameSpace.Faculties>): Direction[] => state[NameSpace.Faculties].filteredDirections;
export const getCourse = (state: Pick<State, NameSpace.Faculties>): number => state[NameSpace.Faculties].course;
export const getBudgetValue = (state: Pick<State, NameSpace.Faculties>): boolean => state[NameSpace.Faculties].budget;
export const getPaidValue = (state: Pick<State, NameSpace.Faculties>): boolean => state[NameSpace.Faculties].paid;
export const getMaxDefaultPrice = (state: Pick<State, NameSpace.Faculties>): number => state[NameSpace.Faculties].maxPrice;
export const getMinDefaultPrice = (state: Pick<State, NameSpace.Faculties>): number => state[NameSpace.Faculties].minPrice;
export const getCurrentPrice = (state: Pick<State, NameSpace.Faculties>): number => state[NameSpace.Faculties].currentValue;
export const getSortType = (state: Pick<State, NameSpace.Faculties>): SortingTitle => state[NameSpace.Faculties].sortedFilter;
export const getActiveFilter = (state: Pick<State, NameSpace.Faculties>): FilterTitle | null | 'Sorting' => state[NameSpace.Faculties].activeFilter;

export const getLoadingStatus = (state: Pick<State, NameSpace.Faculties>): boolean => state[NameSpace.Faculties].loading;
