import {createSlice} from '@reduxjs/toolkit'
import {NameSpace} from "../../const/namespaces";
import {telegramLogin} from "../api/api-action";

export type userState = {
    token: string | null;
}

const initialState: userState = {
    token:null,
};

export const LoginProcess = createSlice({
        name: NameSpace.User,
        initialState: initialState,
        reducers: {
        },
        extraReducers: function (builder) {
            builder.addCase(telegramLogin.fulfilled, (state, action) => {
                state.token = action.payload.access_token;
            })
        }
    }
)

