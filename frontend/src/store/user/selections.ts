import {State} from "../use-app/use-app";
import {NameSpace} from "../../const/namespaces";

export const getToken = (state: Pick<State, NameSpace.User>): string | null => state[NameSpace.User].token;
