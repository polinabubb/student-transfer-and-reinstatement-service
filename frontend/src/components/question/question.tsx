import './question.css';

type QuestionProps = {
    question: string;
    answer: string[];
    key: number;
}

export function Question({question, answer, key}: QuestionProps): JSX.Element {
    return (
        <details className="question-container" key={key}>
            <summary className="question">
                {question}
            </summary>
            <div className="answer">
                {answer.map((part) =>
                    <div className="part-answer" key={part}>
                        {part}
                    </div>)}
            </div>
        </details>
    )
}
