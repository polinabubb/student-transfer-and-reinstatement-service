'use client'
import {NavigationSections, NavigationTitle} from '../../const';
import {NavigationSection} from './types';
import Link from 'next/link';
import {useEffect, useState} from "react";
import './header.css';
import {ThemeIcon} from "../theme-icon/theme-icon";

const StorageKey = 'local-navigation-section';

export function Header(): JSX.Element {
    const [activeNavigationSection, setNavigationSection] = useState<NavigationTitle>(NavigationTitle.Directions);
    const [needGetNavigationInStorage, setNeedGetNavigationInStorage] = useState(true);

    useEffect(() => {
        if (needGetNavigationInStorage) {
            let localNavigationSection = localStorage.getItem(StorageKey) as NavigationTitle;
            if (typeof localNavigationSection === 'undefined') {
                localStorage.setItem(StorageKey, NavigationTitle.Directions);
                localNavigationSection = NavigationTitle.Directions;
            }
            setNavigationSection(localNavigationSection);
            setNeedGetNavigationInStorage(false);
        } else {
            localStorage.setItem(StorageKey, activeNavigationSection);
        }
    }, [activeNavigationSection, needGetNavigationInStorage]);
    const OnClickHandler = (navigationSection: NavigationSection) => () => setNavigationSection(navigationSection.title);

    return (
        <div className="header">
            <div className="navigation-sections--container">
                <div className="navigation-sections">
                    {NavigationSections.map(
                        (navigationSection) => (
                            <Link href={navigationSection.path}
                                  key={navigationSection.path}
                                  className='header-section'
                                  onClick={OnClickHandler(navigationSection)}>
                                <div
                                    className={`navigation-container ${navigationSection.title === activeNavigationSection ? "active" : ""} ${navigationSection.cssTitle}`}>
                                    {navigationSection.title}
                                </div>
                            </Link>
                        )
                    )}
                </div>
            </div>

            <ThemeIcon/>
        </div>
    );
}
