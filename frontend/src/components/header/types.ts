import {AppRoute, CssTitle, NavigationTitle} from "../../const";

export type NavigationSection = {
    title: NavigationTitle;
    path: AppRoute;
    cssTitle: CssTitle;
}