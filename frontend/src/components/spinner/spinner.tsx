'use client'
import {
    DotLoader
} from 'react-spinners';
import './spinner.css';

export function Spinner() {
    return (<div className="spinner">
            <DotLoader color="#8D7DFF"/>
            <div className="spinner-text">
                загрузка
            </div>
        </div>
    );
}

export default Spinner;