'use client'
import 'rc-slider/assets/index.css';
import {ChangeEvent, useState} from 'react';
import './slider.css';
import {useAppDispatch, useAppSelector} from "../../store/use-app/use-app";
import {setPriceValue} from "../../store/faculties/process";
import {getCurrentPrice} from "../../store/faculties/selections";

type SliderProps = {
    maxValue: number;
    minValue: number
}

export function Slider({maxValue, minValue}: SliderProps): JSX.Element {
    const dispatch = useAppDispatch();
    const price = useAppSelector(getCurrentPrice)

    const handleChangeRange = (e: ChangeEvent<HTMLInputElement>) => {
        const numbers = e.target.value;
        dispatch(setPriceValue(parseInt(numbers)));
    };
    const handleChangeNumber = (e: any) => {
        const numbers = e.target.value;
        dispatch(setPriceValue(numbers));

    };

    return (
        <div className="slidecontainer">
            <div className="text-prices">
                <div className="text-price-from-container">
                    <div className="text-price-from">
                        от {minValue}
                    </div>
                </div>
                <div className="text-price-to-container">
                    <div className="text-price-to">
                        до
                        <label className="text-field__label" htmlFor="price"/>
                        <input
                            name="price" id="price"
                            className="text-field__input"
                            type="text"
                            onChange={(e) => handleChangeNumber(e)}
                            value={price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")}
                        />
                    </div>
                </div>
            </div>
            <input type="range" min={minValue} max={maxValue}
                   value={price} className="slider" id="myRange"
                   onChange={(e) => handleChangeRange(e)}
            />
        </div>
    );
}

