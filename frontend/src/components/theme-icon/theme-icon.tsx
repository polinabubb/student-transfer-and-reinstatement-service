'use client'
import {Theme} from '../../const';
import {useEffect, useState} from "react";
//import useTelegramInitData from '../../hooks/useTelegramInitData';

const StorageKey = 'features-color-theme';

/*
const initData = useTelegramInitData();
const tgTheme = {
   theme: initData.webApp?.colorScheme as Theme | Theme.dark,
   isTgTheme: typeof initData.webApp !== 'undefined'
};
*/


export function ThemeIcon(): JSX.Element {
    const [theme, setTheme] = useState<Theme>(Theme.light);
    const [needThemeAnimation, setNeedThemeAnimation] = useState(false);
    const [needGetThemeInStorage, setNeedGetThemeInStorage] = useState(true);
    const handleSwitchTheme = () => {
        if (theme === Theme.dark) {
            setTheme(Theme.light);
        } else {
            setTheme(Theme.dark);
        }
        setNeedThemeAnimation(true);
    };

    useEffect(() => {
        if (needGetThemeInStorage) {
            let localTheme = localStorage.getItem(StorageKey) as Theme;
            if (typeof localTheme === 'undefined') {
                localStorage.setItem(StorageKey, Theme.light);
                localTheme = Theme.light;
            }
            document.documentElement.setAttribute('data-theme', localTheme);
            setTheme(localTheme);
            setNeedGetThemeInStorage(false);
        } else {
            localStorage.setItem(StorageKey, theme);
            document.documentElement.setAttribute('data-theme', theme);
        }

    }, [theme, needGetThemeInStorage]);

    return (
        <button className={`theme-icon${needThemeAnimation ? theme === Theme.dark ? '-light' : '-dark' : ''}`}
                onClick={handleSwitchTheme}>
        </button>
    );
}


export default ThemeIcon;