'use client'
import {Story} from './types'
import './stories.css';
import './price-story/price-story.css';
import './places-story/places-story.css';
//import {PlacesStory} from './places-story/places-story';
//import {PriceStory} from './price-story/price-story';

type StoryContentProps = {
    storyContent: Story;
    crossHandle: () => void;
}
function getPlaceWord(number: string | undefined): string {
    if (number) {
        return `${number} места`;
    }
    return '';

}
export function StoryContent({storyContent, crossHandle}: StoryContentProps): JSX.Element {
    return (
        <div className="story-content">
            <div className="cross" onClick={crossHandle}>&#10008;</div>
            {
                storyContent.view === 'places' ?
                    <div className="places-content">
                        <div className="direction-container">
                            <div className="direction-course">
                                <div className="direction">{storyContent.direction}</div>
                                <div className="course">{storyContent.course}</div>
                            </div>
                        </div>

                        <div className="transcript">{storyContent.transcript}</div>

                        <div className="budget-text">бюджет</div>

                        <div className="budget-numbers">
                            <div
                                className="budget-place-after">  {getPlaceWord(storyContent.places_budget?.places_budget_after)}</div>
                            {storyContent.places_budget?.isChange && <div className="budget-place-before">
                                {storyContent.places_budget?.places_budget_before}
                            </div>
                            }
                        </div>

                        <div className="paid-text">платка</div>
                        <div className="paid-numbers">
                            <div
                                className="paid-place-after">  {getPlaceWord(storyContent.places_paid?.places_paid_after)}</div>
                            {storyContent.places_paid?.isChange && <div className="paid-place-before">
                                {storyContent.places_paid?.places_paid_before}
                            </div>
                            }

                        </div>
                        <div className="places_image"/>

                    </div>
                    : (  <div className="price-content">
                        <div className="price-direction-container">
                            <div className="price-direction-course">
                                <div className="price-direction">{storyContent.direction}</div>
                                <div className="price-course">{storyContent.course}</div>
                            </div>
                        </div>

                        <div className="price-transcript">{storyContent.transcript}</div>

                        <div className="price-numbers">
                            <div className="price-after-container">
                                <div className="price-after"> {storyContent.price_after}</div>
                                <div className="currency">р/год</div>
                            </div>
                            {<div className="price-before">
                                {storyContent.price_before}
                            </div>
                            }
                        </div>

                        <div className="price-image"/>
                    </div>)
            }
        </div>
    );
}
