'use client'
import {useState, useEffect, useCallback} from "react";
import {Story} from './types';
import {StoryContent} from './stories-content';
import './stories.css';

type StoriesProps = {
    stories: Story[];
    currentStory: number;
    crossHandle: () => void;
    setViewedStory: (id: number) => void;
}

export function Stories({stories, currentStory, crossHandle, setViewedStory}: StoriesProps): JSX.Element {
    const [activeStory, setActiveStory] = useState(currentStory);
    const [storyProgress, setStoryProgress] = useState(0);
    const HandleNextClick = useCallback(() => {
        if (stories.length - 1 === activeStory) {
            crossHandle();
        }
        setStoryProgress(0);
        const id = Math.min(stories.length - 1, activeStory + 1);
        setActiveStory(id);
        setViewedStory(id);
    }, [activeStory, crossHandle, stories.length, setViewedStory])

    const HandlePrevClick = useCallback(() => {
        const id = Math.max(0, activeStory - 1);
        setActiveStory(id);
        setViewedStory(id);
        setStoryProgress(0);
    }, [activeStory, setViewedStory])

    useEffect(() => {
        const id = setInterval(() => {
            setStoryProgress(storyProgress + 10);
        }, 1000);
        if (storyProgress === 100) {
            HandleNextClick();
        }
        return () => {
            clearInterval(id);
        }
    }, [storyProgress, HandleNextClick]);
    return (
        <div className="background-stories">
            <div className="player">
                <div className="timeline">
                    {stories.map((story) => (
                        <div className="timeline-chunk" key={story.id}>
                            <div className={`timeline-chunk-inner${story.id !== activeStory ? '' : '-active'}`}
                                 style={{width: `${story.id <= activeStory ? story.id === activeStory ? `${storyProgress}%` : "100%" : "0%"}`}}>
                            </div>
                        </div>
                    ))}
                </div>
                <div className="player-content">
                    <div className="player-chunk-switcher player-chunk-prev"
                         onClick={HandlePrevClick}></div>
                    <div className="player-chunk-switcher player-chunk-next"
                         onClick={HandleNextClick}></div>
                    {stories.map((story) => (
                        <div className={`player-chunk${story.id === activeStory ? "-active" : ""}`} key={story.id}>
                            <StoryContent storyContent={story} crossHandle={crossHandle}/>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
}

