import {Story} from "./types";

export const stories: Story[] = [
    {
        id: 0,
        direction: 'ФИИТ',
        transcript: 'Фундаментальная информатика и информационные технологии',
        course: '2 курс',
        price_before: '221000',
        price_after: '367000',
        view: "price",
        link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
        isViewed: false,
    },
    {
        id: 1,
        direction: 'ФИИТ',
        transcript: 'Фундаментальная информатика и информационные технологии',
        course: '2 курс',
        places_budget: {places_budget_before: '12', places_budget_after: '23', isChange: true},
        places_paid: {places_paid_before: '5', places_paid_after: '154', isChange: true},
        view: "places",
        link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
        isViewed: false,
    },
    {
        id: 2,
        direction: 'ФИИТ',
        transcript: 'Фундаментальная информатика и информационные технологии',
        course: '2 курс',
        price_before: '221000',
        price_after: '367000',
        view: "price",
        link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
        isViewed: false,
    }
];