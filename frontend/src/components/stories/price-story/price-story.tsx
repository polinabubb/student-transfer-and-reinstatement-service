'use client'
import {Story} from '../types'
import './price-story.css';

type PriceStoryProps = {
    storyContent: Story;
}

export function PriceStory({storyContent}: PriceStoryProps): JSX.Element {
    return (
        <div className="price-content">
            <div className="price-direction-container">
                <div className="price-direction-course">
                    <div className="price-direction">{storyContent.direction}</div>
                    <div className="price-course">{storyContent.course}</div>
                </div>
            </div>

            <div className="price-transcript">{storyContent.transcript}</div>

            <div className="price-numbers">
                <div className="price-after-container">
                    <div className="price-after"> {storyContent.price_after}</div>
                    <div className="currency">р/год</div>
                </div>
                {<div className="price-before">
                    {storyContent.price_before}
                </div>
                }
            </div>

            <div className="price-image"/>
        </div>
    );
}
