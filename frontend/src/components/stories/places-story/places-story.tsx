'use client'
import {Story} from '../types'
import './places-story.css';

type PlacesStoryProps = {
    storyContent: Story;
}

function getPlaceWord(number: string | undefined): string {
    if (number) {
        return `${number} места`;
    }
    return '';

}

export function PlacesStory({storyContent, }: PlacesStoryProps): JSX.Element {
    return (
            <div className="places-content">
                <div className="direction-container">
                    <div className="direction-course">
                        <div className="direction">{storyContent.direction}</div>
                        <div className="course">{storyContent.course}</div>
                    </div>
                </div>

                <div className="transcript">{storyContent.transcript}</div>

                <div className="budget-text">бюджет</div>

                <div className="budget-numbers">
                    <div
                        className="budget-place-after">  {getPlaceWord(storyContent.places_budget?.places_budget_after)}</div>
                    {storyContent.places_budget?.isChange && <div className="budget-place-before">
                        {storyContent.places_budget?.places_budget_before}
                    </div>
                    }
                </div>

                <div className="paid-text">платка</div>
                <div className="paid-numbers">
                    <div
                        className="paid-place-after">  {getPlaceWord(storyContent.places_paid?.places_paid_after)}</div>
                    {storyContent.places_paid?.isChange && <div className="paid-place-before">
                        {storyContent.places_paid?.places_paid_before}
                    </div>
                    }

                </div>
                <div className="places_image"/>

            </div>
    );
}
