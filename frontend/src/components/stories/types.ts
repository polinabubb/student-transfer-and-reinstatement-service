export type Story = {
    id: number;
    direction: string;
    transcript: string;
    course: string;
    places_budget?: { places_budget_before?: string; places_budget_after: string; isChange: boolean }
    places_paid?: { places_paid_before?: string; places_paid_after: string; isChange: boolean }
    price_before?: string;
    price_after?: string;
    view: string;
    link: string;
    isViewed: boolean;
}
