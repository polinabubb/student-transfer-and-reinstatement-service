'use client'
import {useEffect, useState} from "react";
import {Stories} from './stories';
import {stories} from './mocks';
import './stories.css';

const StorageKey = 'local-viewed-story';

export function StoryCards(): JSX.Element {
    const [activeStory, setActiveStory] = useState<number | null>(null);
    const [needGetViewedStoryInStorage, setNeedGetiewedStoryInStorage] = useState(true);

    useEffect(() => {
        if (needGetViewedStoryInStorage) {
            let localNavigationSection = localStorage.getItem(StorageKey);
            if (typeof localNavigationSection === 'undefined') {
                localStorage.setItem(StorageKey, 'false false false');
                localNavigationSection = 'false false false';
            }
            if (localNavigationSection === null) {
                localNavigationSection = 'false false false';
            }
            localNavigationSection.split(' ').map((boolStr, id) => {
                stories[id] = {...stories[id], isViewed: boolStr === 'true'};
            })
            setNeedGetiewedStoryInStorage(false);
        } else {
            localStorage.setItem(StorageKey, `${stories[0].isViewed} ${stories[1].isViewed} ${stories[2].isViewed}`);
        }
    }, [activeStory, needGetViewedStoryInStorage]);
    if (activeStory !== null) {
        return (
            <Stories stories={stories} currentStory={activeStory} crossHandle={() => setActiveStory(null)}
                     setViewedStory={(id) => stories[id] = {...stories[id], isViewed: true}}/>
        )
    } else {
        return (
            <div className="story-cards">
                {stories.map((story) => (
                    <div className="story-card" onClick={() => {
                        setActiveStory(story.id);
                        stories[story.id] = {...story, isViewed: true};
                    }
                    } key={story.id}>
                        <div
                            className={`story-card--image ${story.isViewed ? 'viewed-' : ""}story-${story.id}`}
                        ></div>
                    </div>
                ))}
            </div>
        );
    }

}

