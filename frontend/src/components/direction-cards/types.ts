export type Direction = {
    id: number;
    course: number;
    abbreviation: string;
    abbreviationDecoding: string;
    budgetPlaces: number;
    paidPlaces: number;
    price: number;
    link: string;
};
