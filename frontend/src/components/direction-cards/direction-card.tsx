import {Direction} from './types';
import './direction-cards.css';
import Link from "next/link";

type DirectionCardProps = {
    direction: Direction;
};

export function DirectionCard({direction}: DirectionCardProps): JSX.Element {
    return (
        <Link className="direction-card" href={direction.link}>
            <div
                className="direction-card--image"
            ></div>
            <div className="direction-card--content">
                <div className="direction-card--description">
                    <div className="direction-card--abbreviation">{direction.abbreviation}</div>
                    <div className="direction-card--abbreviationDecoding">{direction.abbreviationDecoding}</div>
                </div>
                <div className="direction-card---price">
                    <div className="direction-card---value">  {direction.price}  </div>
                    <div className="direction-card---currency">p/год</div>
                </div>
                <div className="direction-card--places-content">
                    Места
                    <div className="direction-card-places">
                        <div className="direction-card--places-budget">бюджет <div
                            className="direction-card-place-value"> {direction.budgetPlaces}</div></div>
                        <div className="direction-card--places-paid">платка <div
                            className="direction-card-place-value">{direction.paidPlaces}</div></div>
                    </div>
                </div>
            </div>
        </Link>
    );
}
