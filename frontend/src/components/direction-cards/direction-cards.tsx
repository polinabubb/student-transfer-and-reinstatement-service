'use client'
import {DirectionCard} from './direction-card';
import './direction-cards.css';
import {useAppSelector} from "../../store/use-app/use-app";
import {getFilteredFilms} from "../../store/faculties/selections";

export function DirectionCards(): JSX.Element {
    const directions = useAppSelector(getFilteredFilms)
    if (directions.length > 0) {
    return (
        <div className="direction-cards">
            {directions.map((direction) => <DirectionCard key={direction.id} direction={direction}/>)}
        </div>
    );
    }
    return (<div className="empty-directions-text">Ничего не нашлось по вашему запросу :(</div>)
}


