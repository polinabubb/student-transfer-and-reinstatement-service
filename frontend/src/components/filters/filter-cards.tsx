import {FilterTitle, SortingTitle} from '../../const';
import {Slider} from '../slider/slider';
import './filters.css';
import {useAppDispatch, useAppSelector} from "../../store/use-app/use-app";
import {
    getActiveFilter,
    getBudgetValue,
    getCourse,
    getMaxDefaultPrice,
    getMinDefaultPrice,
    getPaidValue,
    getSortType
} from "../../store/faculties/selections";
import {
    doPlaceFilter,
    doPriceFilter,
    doSort,
    setActiveFilter,
    setBudgetPlace,
    setCourse,
    setPaidPlace
} from "../../store/faculties/process"
import {FacultiesFilters} from "../../types/faculties-filters";
import {useEffect, useRef} from "react";

export function FilterCoastCard(): JSX.Element {
    const dispatch = useAppDispatch();
    const maxPrice = useAppSelector(getMaxDefaultPrice);
    const minPrice = useAppSelector(getMinDefaultPrice);
    const activeFilter = useAppSelector(getActiveFilter)
    const rootRef = useRef(null);
    useEffect(() => {
        const onClick = (e: MouseEvent) => {
            if (activeFilter === FilterTitle.coast && rootRef.current && e.target instanceof Node && !(rootRef.current as HTMLElement)?.contains(e.target)) {
                dispatch(setActiveFilter(null));
            }
        };
        document.addEventListener('click', onClick);
        return () => document.removeEventListener('click', onClick);
    }, [activeFilter, dispatch]);
    return (
        <div className="filters-card" ref={rootRef}>
            <div className="filter-sorting-cards-content">
                <Slider maxValue={maxPrice} minValue={minPrice}/>
                <button className="button" onClick={() => {
                    dispatch(doPriceFilter())
                    dispatch(setActiveFilter(null))
                }}>
                    <div className="button-text">
                        применить
                    </div>
                </button>
            </div>
        </div>

    );
}

export function SortingCard(): JSX.Element {
    const dispatch = useAppDispatch();
    const type = useAppSelector(getSortType);
    const activeFilter = useAppSelector(getActiveFilter)
    const rootRef = useRef(null);
    useEffect(() => {
        const onClick = (e: MouseEvent) => {
            if (activeFilter === "Sorting" && rootRef.current && e.target instanceof Node && !(rootRef.current as HTMLElement)?.contains(e.target)) {
                dispatch(setActiveFilter(null));
            }
        };
        document.addEventListener('click', onClick);
        return () => document.removeEventListener('click', onClick);
    }, [activeFilter, dispatch]);
    return (
        <div className="sorting-card" ref={rootRef}>
            <div className="filter-sorting-cards-content">
                {[SortingTitle.coast, SortingTitle.freePlace, SortingTitle.allPlace, SortingTitle.paidPlaces]
                    .map((sortingTitle) => (
                            <div key={sortingTitle} className="radio">
                                <input className="custom-radio" type="radio" id={sortingTitle} name="sorting"
                                       checked={sortingTitle === type}
                                       onClick={() => dispatch(doSort(sortingTitle))}
                                ></input>
                                <label htmlFor={sortingTitle}>{sortingTitle}</label>
                            </div>
                        )
                    )}
            </div>
        </div>
    );
}

export function FilterPlacesCard(): JSX.Element {
    const dispatch = useAppDispatch();
    const budget = useAppSelector(getBudgetValue);
    const paid = useAppSelector(getPaidValue);
    const activeFilter = useAppSelector(getActiveFilter)
    const rootRef = useRef(null);
    useEffect(() => {
        const onClick = (e: MouseEvent) => {
            if (activeFilter === FilterTitle.place && rootRef.current && e.target instanceof Node && !(rootRef.current as HTMLElement)?.contains(e.target)) {
                dispatch(setActiveFilter(null));
            }
        };
        document.addEventListener('click', onClick);
        return () => document.removeEventListener('click', onClick);
    }, [activeFilter, dispatch]);
    return (
        <div className="filters-card" ref={rootRef}>
            <div className="filter-sorting-cards-content">
                {[FacultiesFilters.budget, FacultiesFilters.paid]
                    .map((sortingTitle) => (
                            <div key={sortingTitle} className="checkbox">
                                <input className="custom-checkbox" type="checkbox" id={sortingTitle} name="place"
                                       checked={sortingTitle === FacultiesFilters.budget && budget || sortingTitle === FacultiesFilters.paid && paid}
                                       onChange={(e) => {
                                           if (sortingTitle === FacultiesFilters.budget)
                                               dispatch(setBudgetPlace(e.target.checked))
                                           else {
                                               dispatch(setPaidPlace(e.target.checked))
                                           }
                                       }}
                                ></input>
                                <label htmlFor={sortingTitle}>{sortingTitle}</label>
                            </div>
                        )
                    )}
                <button className="button" onClick={() => dispatch(doPlaceFilter())}>
                    <div className="button-text">
                        применить
                    </div>
                </button>
            </div>
        </div>
    );
}

export function FilterCoursesCard(): JSX.Element {
    const dispatch = useAppDispatch();
    const currentCourse = useAppSelector(getCourse)
    const activeFilter = useAppSelector(getActiveFilter)
    const rootRef = useRef(null);
    useEffect(() => {
        const onClick = (e: MouseEvent) => {
            if (activeFilter === FilterTitle.course && rootRef.current && e.target instanceof Node && !(rootRef.current as HTMLElement)?.contains(e.target)) {
                dispatch(setActiveFilter(null));
            }
        };
        document.addEventListener('click', onClick);
        return () => document.removeEventListener('click', onClick);
    }, [activeFilter, dispatch]);
    return (
        <div className="filters-card" ref={rootRef}>
            <div className="filter-sorting-cards-content">
                {[1, 2, 3, 4].map((course) => (
                    <div key={course} className="radio">
                        <input
                            className="custom-radio"
                            type="radio"
                            id={course.toString()}
                            name="course"
                            checked={currentCourse === course}
                            onChange={(event) => {
                                dispatch(setCourse(course))
                                dispatch(setActiveFilter(null))
                            }}
                        ></input>
                        <label htmlFor={course.toString()}>{course}</label>
                    </div>
                ))}
            </div>
        </div>
    );
}
