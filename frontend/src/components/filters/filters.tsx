'use client'
import {FilterTitle, SortingTitle} from "../../const";
import {FilterCoastCard, FilterCoursesCard, FilterPlacesCard, SortingCard} from './filter-cards';
import './filters.css';
import {useAppDispatch, useAppSelector} from "../../store/use-app/use-app";
import {setActiveFilter} from "../../store/faculties/process";
import {getActiveFilter} from "../../store/faculties/selections";

export function Filters(): JSX.Element {
    const dispatch = useAppDispatch()
    const activeFilter = useAppSelector(getActiveFilter)
    const onClickHandler = (checkedFilter: FilterTitle) => () => {
        switch (activeFilter) {
            case checkedFilter:
                dispatch(setActiveFilter(null));
                break;
            default:
                dispatch(setActiveFilter(checkedFilter));
        }
    }
    const onClickHandlerSorting = () => {
        switch (activeFilter) {
            case 'Sorting':
                dispatch(setActiveFilter(null));
                break;
            default:
                dispatch(setActiveFilter('Sorting'));
        }
    }
    return (
        <div className="filters-sorting-sections">
            {[FilterTitle.course, FilterTitle.coast, FilterTitle.place].map((filter) =>
                (<div key={filter} className="filter-section-container" onClick={onClickHandler(filter)}>
                    <div className="filter-section">
                        <div className="filter-title">  {filter} </div>
                        <div className="check-mark-container"></div>
                    </div>
                </div>))}
            {
                activeFilter === FilterTitle.coast && <FilterCoastCard/>
            }
            {
                activeFilter === FilterTitle.place && <FilterPlacesCard/>
            }
            {
                activeFilter === FilterTitle.course && <FilterCoursesCard/>
            }
            <div className="sorting-container">
                <div className="sorting-section-container" onClick={onClickHandlerSorting}>
                    <div className="sorting-section">
                        < div className="sorting-image-container"></div>
                        <div className="sorting-title"> {SortingTitle.freePlace}
                        </div>
                    </div>
                </div>
                {
                    activeFilter === 'Sorting' && <SortingCard/>
                }
            </div>
        </div>
    )
        ;
}
