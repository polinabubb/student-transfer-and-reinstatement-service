export type step = {
    stepParts: {
        text: string;
        isLink: boolean;
        link?: string;
        isLineBreak: boolean;
    }[],
    additionToStep?:
        {
            text: string; isLineBreak: boolean;
        }[]
};

export type Checklist = {
    id: number;
    name: string;
    steps: step[];
    requirements: string[];

    finalRecommendation?: step[];
    additional_info?: { link: string; description: string }[]
};