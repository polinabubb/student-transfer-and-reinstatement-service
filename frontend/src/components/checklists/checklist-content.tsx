'use client'
import {Checklist} from './types';
import './checklists.css';
import Link from "next/link";

type ChecklistContentProps = {
    checklist: Checklist;
}

export function ChecklistContent({checklist}: ChecklistContentProps): JSX.Element {
    return (
        <div className='checklist-content'>
            <details key={`requirements-container-${checklist}`} className='details-checklist-content'>
                <summary className='requirements-title'>
                    обязательные условия
                </summary>
                <div
                    className='requirements'>
                    {checklist.requirements.map((requirement, index) =>
                        <div className='requirement' key={index}>
                            <div className='requirement-index'>{`${index + 1}.`}</div>
                            <div className='requirement-text'>{requirement}</div>
                        </div>
                    )}
                </div>
            </details>


            <div className='checklist-points'>
                {checklist.steps
                    .map((step, index) => (
                            <div key={step.stepParts.join(' ') + checklist.name} className='checkbox-cheklist'>
                                <input className='custom-checkbox-cheklist' type="checkbox"
                                       id={checklist.name + index}
                                       name="steps"
                                ></input>
                                <label className='cheklist-label' htmlFor={checklist.name + index}>
                                    <div className="step">
                                        {step.stepParts.map((part) =>
                                            <>
                                                {part.isLink ? <a className='link-part' href={part.link}>
                                                        {part.text}
                                                    </a> :
                                                    part.text}
                                                {part.isLineBreak && <br/>}
                                            </>
                                        )}
                                    </div>
                                </label>
                                <div className="addition-to-step">
                                    {step.additionToStep?.map((part) =>
                                        < >
                                            {part.text}
                                            {part.isLineBreak && <br/>}
                                        </>
                                    )}
                                </div>
                            </div>
                        )
                    )}
            </div>
            {checklist.finalRecommendation &&
                <div className="final-recommendation-container">
                    что потом:
                    <div className="final-recommendation">

                        {checklist.finalRecommendation.map((step) =>
                            <div className="recommendation-step" key={step.stepParts.join(' ')}>
                                {step.stepParts.map((sentence) =>
                                    <>
                                        {sentence.isLink ? <Link className='link-part' href={String(sentence.link)}>
                                                {sentence.text}
                                            </Link> :
                                            sentence.text}
                                        {sentence.isLineBreak && <br/>}
                                    </>)}</div>)
                        }
                    </div>
                </div>}
            <div className='additional-infos'>
                {checklist.additional_info?.map((additional_info) =>
                    <Link key={additional_info.description} className='additional-info' href={additional_info.link}>
                        <div className='additional-image-container' title={additional_info.description}/>
                        <div className='additional-description'> {additional_info.description} </div>
                    </Link>
                )
                }
            </div>
        </div>
    )
}

