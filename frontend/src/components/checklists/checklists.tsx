'use client'
import {checklist, generalChecklistInfo} from '@components/checklists/mocks';
import {ChecklistContent} from '@components/checklists/checklist-content';
import './checklists.css';
import Link from "next/link";

export function Checklists(): JSX.Element {
    return (
        <div className="checklist-info">
            <div className="checklists">
                {checklist.map((list) => (
                    <details className="checklist-container" key={`mocks-container-${list.id}`}>
                        <summary className="checklist-name">
                            {list.name}
                        </summary>
                        <ChecklistContent checklist={list}/>
                    </details>
                ))}
            </div>
            <details className="general-checklist-container">
                <summary className="general-checklist-name">
                    о подаче заявлений
                </summary>
                <div className="general-checklists">
                    {generalChecklistInfo.map((point) =>
                        <div className="general-checklist" key={`${point}`}>
                            {point}
                        </div>
                    )}
                </div>

            </details>
            <div className="footer">
                <div className="footer-text">
                    Если остались вопросы, позвоните в деканат <Link className="tel" href="tel:+73433899467">+7 (343)
                    389-94-67</Link>
                </div>
            </div>
        </div>
    )
}

