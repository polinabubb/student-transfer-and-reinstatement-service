import {Checklist} from './types';

export const checklist: Checklist[] = [
    {
        id: 1,
        name: 'Перевод на бюджет',
        requirements: [
            'Есть вакантныe бюджетные места',
            'Нет академ задолженности и долгов по оплате',
            'Два последних семестра закрыты на 4 и 5, либо студент остался без попечения родителей, либо утратил одного из родителей в период обучения'
        ],
        steps: [
            {
                stepParts: [{
                    text: 'Распечатать и заполнить заявление', isLink: false,
                    isLineBreak: false,
                }]
            },
            {
                stepParts: [
                    {
                        text: 'Распечатать снимок экрана всей страницы раздела ', isLink: false,
                        isLineBreak: false,
                    }, {
                        text: '«Финансовые сервисы»',
                        isLink: true,
                        link: 'https://ubu.urfu.ru/fse/contract-information',
                        isLineBreak: false,//чтобы подтвердить отсутствие задолженностей
                    }]
            },
            {
                stepParts: [{
                    text: 'Проверить, что в зачетке нет задолженностей по предметам и всё закрыто на 4 и 5',
                    isLink: false,
                    isLineBreak: false,
                }]
            },
            {
                stepParts: [{
                    text: 'Написать характеристику, где вы себя хвалите и отправить Пьянзиной Е.С.', isLink: false,
                    isLineBreak: true,
                }],
                additionToStep: [{
                    text: ' называйте себя в формате фамилия-инициалы или он/она',
                    isLineBreak: false,
                }]
            },// она проверит и распечатает при необходимости отредактирует
            {
                stepParts: [{
                    text: 'Прийти в деканат с заготовленными документами и подать заявление', isLink: false,
                    isLineBreak: false,
                }]
            },
        ],
        finalRecommendation: [{
            stepParts: [{
                text: 'Ждать и иногда звонить/заходить в деканат, потому что Вас не будут уведомлять состоялся ли перевод',
                isLink: false,
                isLineBreak: false
            }]
        }],
        additional_info: [
            {
                link: 'https://hti.urfu.ru/fileadmin/user_upload/site_15078/dokumenty/blanki_zajavlenii/Forma_No19_perevod_na_bjudzhet.pdf',
                description: 'заявление'
            },
            {
                link: 'https://urfu.ru/sveden/vacant/',
                description: 'Вакантные места, регламенты, положения'
            },
            {
                link: 'https://urfu.ru/fileadmin/user_upload/urfu.ru/documents/education/2016/No_SMK-PVD-7.5.1-01-95-2016_Polozhenie_o_perekhode_s_platnogo_obuchenija_na_besplatnoe__888524_v1_.pdf',
                description: 'Официальный приказ'
            },
            {
                link: 'https://www.notion.so/fiit-urfu/d44a33c5ebaf487faeb335402b3ff9af',
                description: 'Ноушн с подробными инструкциями'
            },
        ]
    },
    {
        id: 2,
        name: 'Перевод на другое направление',
        requirements: [
            'Академ разница между зачёткой и учебным планом ≤ 15 зачётных единиц',
            'Есть вакантные места'
        ],
        steps: [
            {
                stepParts: [{
                    text: 'Пообщаться с Еленой Ивановной (деканат, правая дверь) или Еленой Сергеевной (деканат, левая дверь) в приёмные часы',
                    isLink: false,
                    isLineBreak: true,
                },
                ],
                additionToStep: [{
                    text: 'Они найдут академ разницу вашей зачётки и программы желаемого направления',

                    isLineBreak: false,
                }]
            },
            {
                stepParts: [{
                    text: 'Посмотреть количество вакантных мест', isLink: false,
                    isLineBreak: false,
                }]
            },//нужный курс удалить?
            {
                stepParts: [{
                    text: 'Распечатать и заполнить заявление', isLink: false,
                    isLineBreak: false,
                }]
            },
            {
                stepParts: [{
                    text: 'Прийти в деканат с заготовленными документами и подать заявление', isLink: false,
                    isLineBreak: false,
                }]
            },
            {
                stepParts: [{
                    text: 'Если Вам позвонили и сказали, что перевод прошёл успешно, получите в деканате положение об академ разнице',
                    isLink: false,
                    isLineBreak: true,
                }],
                additionToStep: [{
                    text: 'в нем будут: перезачёты, академ разница и срок её устранения',
                    isLineBreak: true,
                }, {
                    text: 'Иначе либо в переводе отказано, либо приказ ещё не выпущен. Уточнить можно в деканате',
                    isLineBreak: false,
                }]
            },

        ],
        additional_info: [
            {
                link: 'https://urfu.ru/sveden/vacant/',
                description: 'Вакантные места, регламенты, положения'
            },
            {
                link: 'https://urfu.ru/fileadmin/user_upload/urfu.ru/documents/education/prikaz_po_osnovnoj_deyatelnosti_1115_03_ot_19_12_2022_vedenie_v_dejstvie_polozhe.pdf',
                description: 'Перевод в деталях: стр. 10-21 '
            },
            {
                link: 'https://gsem.urfu.ru/fileadmin/user_upload/urfu.ru/documents/forms/Forma_No6_Zajavlenie_na_perevod_vnutri_vuza.pdf',
                description: 'заявление'
            },
        ]
    },
    {
        id: 3,
        name: 'Выход в академический отпуск',
        requirements: ['Иметь основания для предоставления отпуска'],
        steps: [
            {
                stepParts: [{
                    text: 'Распечатать документы, подтверждающие основания для отпуска (пункт 5.2 в списке документов)',
                    isLink: false,
                    isLineBreak: false,
                }]
            },
            {
                stepParts: [{
                    text: 'Прийти в деканат с заготовленными документами и подать заявление', isLink: false,
                    isLineBreak: false,
                }]
            },
        ],
        finalRecommendation: [{
            stepParts: [{
                text: 'Ждать и иногда звонить/заходить в деканат, потому что Вас не будут уведомлять о выдаче или отклонении отпуска',
                isLink: false,
                isLineBreak: false
            }]
        }],
        additional_info: [
            {
                link: 'https://aspirant.urfu.ru/fileadmin/user_upload/site_15796/polozheniya/Polozhenie_o_porjadke_i_osnovanijakh_predostavlenija_otpuskov_obuchajushchimsja.pdf',
                description: 'Список документов'
            },
        ]
    },
    {
        id: 4,
        name: 'Выход из академического отпуска',
        requirements: ['Подать заявление не позднее 10 дней после окончания академ отпуска'],
        steps: [
            {
                stepParts: [{
                    text: 'Распечатать документы, подтверждающие, что Вы можете продолжить учёбу (пункт 6.2 в списке документов)',
                    isLink: false,
                    isLineBreak: false,
                }]
            },
            {
                stepParts: [{
                    text: 'Взять с собой заготовленные документы, копию паспорта, приписное/военный билет(если вы мужчина) и подать заявление',
                    isLink: false,
                    isLineBreak: false,
                }]
            },
        ],
        finalRecommendation: [{
            stepParts: [{
                text: 'Ждать и иногда звонить/заходить в деканат, потому что Вас не будут уведомлять о выходе приказа',
                isLink: false,
                isLineBreak: true,
            }
            ]
        }, {
            stepParts: [{
                text: 'Также можно проверять поле ', isLink: false,
                isLineBreak: false,
            }, {
                text: '«Статус»', isLink: true, link: 'https://id.urfu.ru/ProfileManagement/Profile/',
                isLineBreak: false,
            }, {
                text: ' когда приказ будет выпущен оно поменяется с «Академ. отпуск» на «Активный»',
                isLink: false,
                isLineBreak: false,
            }]
        }],
        additional_info: [
            {
                link: 'https://aspirant.urfu.ru/fileadmin/user_upload/site_15796/polozheniya/Polozhenie_o_porjadke_i_osnovanijakh_predostavlenija_otpuskov_obuchajushchimsja.pdf',
                description: 'Список документов'
            },
        ]
    }
]

export const generalChecklistInfo: string[] = [
    'Куда: в правую дверь деканата матмеха к Светлане Владимировне, Тургенева 4, 6 этаж',//секретарю
    'Время обработки: около 14 дней, но подавать лучше за месяц до начала семестра',//, чтобы всё успели обработать и выпустить приказ
    'Исправления в заявлениях не допускаются, Лучше принести частично заполненное, лично уточнить и дописать'
]