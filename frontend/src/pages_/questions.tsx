'use client'
import {Question} from '@components/question/question';
import {useAppDispatch, useAppSelector} from "../store/use-app/use-app";
import {fetchQuestions} from "../store/api/api-action";
import {getQuestions} from "../store/questions/selections";
import {useEffect} from "react";

export function Questions(): JSX.Element {
    const dispatch = useAppDispatch();
    useEffect(() => {
        dispatch(fetchQuestions());
    }, [dispatch]);

    const questions = useAppSelector(getQuestions);
    return (
        <>
            {questions.map((question) => (
                <Question question={question.question} answer={question.answer} key={question.id}/>
            ))}
        </>
    )
}
