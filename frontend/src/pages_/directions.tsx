'use client';
import {DirectionCards} from '@components/direction-cards/direction-cards'
import {Filters} from '@components/filters/filters';
import {StoryCards} from '@components/stories/story-cards';
import {useAppDispatch, useAppSelector} from "../store/use-app/use-app";
import {getLoadingStatus} from "../store/faculties/selections";
import {fetchAllFaculties} from "../store/api/api-action";
import {Spinner} from '@components/spinner/spinner';
import {useEffect} from "react";

export function DirectionsPage(): JSX.Element {
    const dispatch = useAppDispatch();
    useEffect(() => {
        dispatch(fetchAllFaculties());
    }, [dispatch])

    const isLoading = useAppSelector(getLoadingStatus);
    if (isLoading) {
        return <Spinner/>
    }
    return (
        <>
            <StoryCards/>
            <Filters/>
            {/*{token ?? <div>{token}</div>}*/}
            <DirectionCards/>
        </>
    )
}

