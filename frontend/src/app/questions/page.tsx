'use client'
import {Questions} from '../../pages_/questions';
import ProviderWrapper from "@app/ProviderWrapper";

export default function Page() {
    return (
        <ProviderWrapper><Questions/></ProviderWrapper>
    )
}
