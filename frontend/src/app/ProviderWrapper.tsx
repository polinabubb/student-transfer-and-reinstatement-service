import React from 'react';
import {store} from '../store';
import {Provider} from "react-redux";

// @ts-ignore
const ProviderWrapper = ({children}) => {
    return (
        <Provider store={store}>
            {children}
        </Provider>
    );
};

export default ProviderWrapper;
