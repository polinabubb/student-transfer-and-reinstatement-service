'use client'
import {DirectionsPage} from '../pages_/directions';
import {store} from '../store'
import useTelegramInitData from "../hooks/useTelegramInitData";
import {Login} from "../types/request/login";
import {telegramLogin} from "../store/api/api-action";
import ProviderWrapper from "@app/ProviderWrapper";

export default function Home(): JSX.Element {
    // const initData = useTelegramInitData();
    // if (initData) {
    //     const login: Login = {
    //         id: initData.user?.id,
    //         is_bot: false,
    //         first_name: initData.user?.first_name,
    //         last_name: initData.user?.last_name,
    //         username: initData.user?.usernames,
    //         language_code: initData.user?.language_code,
    //         is_premium: false,
    //         added_to_attachment_menu: false,
    //         allows_write_to_pm: false,
    //         photo_url: ''
    //     }
    //     store.dispatch(telegramLogin(login))
    // }
  return (
      <main>
          <ProviderWrapper>
              {/*<div>{initData.user?.usernames}</div>*/}
              <DirectionsPage/></ProviderWrapper>
      </main>
  )
}
