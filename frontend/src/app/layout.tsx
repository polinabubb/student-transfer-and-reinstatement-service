import type {Metadata} from 'next'
import {Inter} from 'next/font/google'
import './globals.css'
import {Header} from '@components/header/header';
import React from 'react';
//import {Providers} from './providers'
import Script from 'next/script';
//import ThemeIcon from '../components/theme-icon/theme-icon';

const inter = Inter({subsets: ['latin']})
export const metadata: Metadata = {
    title: 'Student helper',
    description: 'Student helper by Vobladogs',
}
export default function RootLayout({children,}: { children: React.ReactNode }) {
    return (

        <html lang="en" suppressHydrationWarning>
        <head>
            <Script src="https://telegram.org/js/telegram-web-app.js"
                    strategy="beforeInteractive"/>
            <title>Student Helper</title>
        </head>
        <body className={inter.className}>

        <Header/>{children}
        </body>
        </html>

    )
}
/*
 <ThemeIcon>  <Script src="https://telegram.org/js/telegram-web-app.js"
                    strategy="beforeInteractive"/>
 */
