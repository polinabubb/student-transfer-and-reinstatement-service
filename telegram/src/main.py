import asyncio
import logging
import sys

from aiogram import Bot, Dispatcher
from aiogram.enums import ParseMode
from aiogram.fsm.storage.memory import MemoryStorage
from config.settings import get_settings
from handlers.default import default_router
from handlers.questions import questions_router

TOKEN = get_settings().TELEGRAM_TOKEN

DEBUG = get_settings().DEBUG

storage = MemoryStorage()

dp = Dispatcher(storage=storage)


async def main():
    bot = Bot(token=TOKEN, parse_mode=ParseMode.HTML)
    dp = Dispatcher(storage=MemoryStorage())
    dp.include_router(default_router)
    dp.include_router(questions_router)
    await bot.delete_webhook(drop_pending_updates=True)
    await dp.start_polling(bot, allowed_updates=dp.resolve_used_update_types())


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    asyncio.run(main())

