from pydantic import BaseModel


class QuestionSearchResponse(BaseModel):
    id: int
    question: str
    answer: str
    is_frequent: bool
    count: int
    tags: list[str]
    similarity_ratio: float
