import httpx
from client.dto import QuestionSearchResponse
from config.settings import get_settings


class QuestionsService:
    def __init__(self):
        self.host_api_url = get_settings().HOST_API_URL

    async def search(self, msg: str) -> list[QuestionSearchResponse] | None:
        try:
            async with httpx.AsyncClient() as client:
                resp = await client.get(f"{self.host_api_url}/api/v1/questions/search?query={msg}&method=trgm")
                return [QuestionSearchResponse.model_validate(question) for question in resp.json()]
        except httpx.RequestError:
            return None
