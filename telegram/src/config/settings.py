from enum import Enum
from functools import lru_cache

from pydantic_settings import BaseSettings, SettingsConfigDict


class ModeEnum(str, Enum):
    DEVELOPMENT = "DEVELOPMENT"
    PRODUCTION = "PRODUCTION"
    TESTING = "TESTING"


class Settings(BaseSettings):
    """
    Application settings.

    These parameters can be configured
    with environment variables.
    """

    model_config = SettingsConfigDict(env_file=".env", case_sensitive=True, env_file_encoding="utf-8")

    MODE: ModeEnum = ModeEnum.DEVELOPMENT
    PROJECT_NAME: str = "Backend"
    DEBUG: bool = True
    TELEGRAM_TOKEN: str
    HOST_API_URL: str
    TG_CHAT_LINK: str
    TG_CHAT_ID: int


@lru_cache
def get_settings() -> Settings:
    settings = Settings()
    return settings
