from aiogram.fsm.state import State, StatesGroup


class QuestionState(StatesGroup):
    choose_question = State()
    get_answer = State()
