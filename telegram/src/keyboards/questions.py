from aiogram.utils.keyboard import InlineKeyboardBuilder
from client.dto import QuestionSearchResponse


def get_questions_kbd(questions: list[QuestionSearchResponse]):
    builder = InlineKeyboardBuilder()
    for idx, question in enumerate(questions[:3]):
        builder.button(text=str(idx + 1), callback_data=f"id:{question.id}")
    builder.button(text='-', callback_data=f"id:{-1}")
    return builder
