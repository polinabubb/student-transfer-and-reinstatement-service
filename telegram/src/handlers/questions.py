from aiogram import Router
from aiogram.fsm.context import FSMContext
from aiogram.types import CallbackQuery, Message
from client.questions import QuestionsService
from config.settings import get_settings
from keyboards.questions import get_questions_kbd
from replies import Replies, get_formatted_answer, get_question_pool
from states.questions import QuestionState

questions_router = Router()


def get_questions_service() -> QuestionsService:
    return QuestionsService()


@questions_router.message()
async def message_handler(msg: Message, state: FSMContext,
                          questions_service: QuestionsService = get_questions_service()):
    await state.clear()
    similar_questions = await questions_service.search(msg.text)
    await msg.forward(get_settings().TG_CHAT_ID)
    if not similar_questions:
        return await msg.answer(Replies.NOT_FOUND_QUESTION)
    await msg.answer(Replies.CHOOSE_ONE_OF_QUESTIONS + get_question_pool(similar_questions), reply_markup=get_questions_kbd(similar_questions).as_markup())
    await state.set_state(QuestionState.choose_question)
    await state.set_data({'questions': {question.id: question for question in similar_questions}})


@questions_router.callback_query(QuestionState.choose_question)
async def choose_question(callback_query: CallbackQuery, state: FSMContext):
    await callback_query.message.delete_reply_markup()
    question_id = int(callback_query.data.split(':')[1])
    if question_id == -1:
        return await callback_query.message.answer(Replies.NO_SUITABLE_QUESTION)
    data = await state.get_data()
    answer = data['questions'][question_id].answer
    await callback_query.message.answer(get_formatted_answer(answer))
    await state.clear()
