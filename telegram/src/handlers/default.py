from aiogram import Router
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.types import Message
from replies import Replies

default_router = Router()


@default_router.message(Command("start"))
async def start(msg: Message, state: FSMContext):
    await state.set_state(None)
    await msg.answer(Replies.START)


@default_router.message(Command("help"))
async def help(msg: Message):
    await msg.answer(Replies.HELP)
