[tool.poetry]
name = "backend"
version = "0.1.0"
description = ""
authors = ["Artyom Romanov <artem.romanov.03@bk.ru>"]
readme = "README.md"

[tool.poetry.dependencies]
python = "^3.11"
fastapi = "^0.104.1"
uvicorn = "^0.24.0"
sqlalchemy = "^2.0.23"
pydantic-settings = "^2.1.0"
alembic = "^1.12.1"
asyncpg = "^0.29.0"
python-jose = {extras = ["cryptography"], version = "^3.3.0"}
python-multipart = "^0.0.6"
validators = "^0.22.0"
passlib = "^1.7.4"
beautifulsoup4 = "^4.12.2"

[tool.poetry.group.dev.dependencies]
ruff = "^0.1.5"
isort = "^5.12.0"
mypy = "^1.7.0"
black = "^23.11.0"
pylint = "^3.0.2"
flake8 = "^6.1.0"
flake8-todo = "^0.7"
pytest = "^7.4.2"
pytest-asyncio = "^0.21.1"
pytest-cov = "^4.1.0"
pytest-randomly = "^3.15.0"
pytest-profiling = "^1.7.0"
pytest-timeout = "^2.2.0"
pytest-freezer = "^0.4.8"
httpx = "^0.25.1"
faker = "^20.0.0"
bandit = "^1.7.5"

[build-system]
requires = ["poetry-core"]
build-backend = "poetry.core.masonry.api"

[tool.poetry.urls]
"Homepage" = "https://gitlab.com/vobladogs/backend"
"Bug Tracker" = "https://gitlab.com/vobladogs/backend/issues"

[tool.black]
line-length = 120
target-version = [ "py311" ]
exclude = '''
(
  /(
      \.eggs
    | \.git
    | \.hg
    | \.mypy_cache
    | \.tox
    | \.venv
    | _build
    | buck-out
    | build
    | dist
  )/
)
'''

[tool.ruff]
line-length = 120
exclude = [
  ".git", "__pycache__", ".mypy_cache", ".pytest_cache",
  "src/db/migrations/*",
]
select = [
    "E",  # pycodestyle errors
    "W",  # pycodestyle warnings
    "F",  # pyflakes
    "I",  # isort
    "C",  # flake8-comprehensions
    "B",  # flake8-bugbear
]
ignore = [
    "B904",
    "B006",
    "E501",  # line too long, handled by black
    "B008",  # do not perform function calls in argument defaults
    "C901",  # too complex
]

[tool.ruff.per-file-ignores]
"__init__.py" = ["F401"]

[tool.ruff.isort]
known-third-party = ["fastapi", "pydantic", "starlette"]

[tool.mypy]
plugins = ["sqlalchemy.ext.mypy.plugin", "pydantic.mypy"]
python_version = "3.11"
warn_return_any = true
warn_unused_configs = true
ignore_missing_imports = true
exclude = ["alembic", "__pycache__", "^config\\.py$", "^env\\.py$",]

[tool.isort]
skip_glob = ["src/db/*"]

[tool.pytest.ini_options]
addopts = "--strict-markers -W error"
junit_family = "xunit2"
testpaths = ["src/tests"]
xfail_strict = true
filterwarnings = [
    "error",
    "error::DeprecationWarning"
]

[tool.flake8]
max-line-length = 120
extend-exclude = [
    'migrations/*',
    'venv/*',         # 'venv' not in default ingore list
]
classmethod-decorators = [
    'validator',
    'root_validator',
    'declared_attr',
]
