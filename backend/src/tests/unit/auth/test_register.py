import uuid


def test_generate_access_token(freezer, jwt_auth):
    freezer.move_to("2023-01-11 00:00:00")
    device_id = str(uuid.uuid4())
    subject = "tester123"
    token = jwt_auth.generate_access_token(subject, {"device_id": device_id})
    decoded_token = jwt_auth.decode_token(token)
    assert decoded_token["iss"] == "FastAPI"
    assert decoded_token["sub"] == subject
    assert decoded_token["type"] == "ACCESS"
    assert decoded_token["iat"] == 1673395200
    assert decoded_token["nbf"] == 1673395200
    assert decoded_token["exp"] == 1673398800
