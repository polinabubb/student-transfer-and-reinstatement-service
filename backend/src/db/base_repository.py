import uuid
from typing import Any, Generic, TypeVar, Union

from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel
from sqlalchemy import delete, select, text
from sqlalchemy.ext.asyncio import AsyncSession

ModelType = TypeVar("ModelType", bound=Any)
CreateSchemaType = TypeVar("CreateSchemaType", bound=BaseModel)
UpdateSchemaType = TypeVar("UpdateSchemaType", bound=BaseModel)


class BaseRepository(Generic[ModelType, CreateSchemaType, UpdateSchemaType]):
    """Base CRUD repository."""

    model: ModelType = None

    def __init__(self, db_session: AsyncSession):
        self.db_session = db_session

    async def create(self, model_in: CreateSchemaType) -> ModelType:
        db_model = self.model(**model_in.model_dump())
        self.db_session.add(db_model)
        await self.db_session.commit()
        await self.db_session.refresh(db_model)
        return db_model

    async def get(self, id: int | str | uuid.UUID) -> ModelType | None:
        db_model = await self.db_session.get(self.model, id)
        return db_model

    async def get_all(self) -> list[ModelType]:
        stmt = select(self.model)
        models = await self.db_session.execute(stmt)
        return models.unique().scalars().all()

    async def get_multi(self, *, skip: int = 0, limit: int = 100) -> list[ModelType]:
        stmt = select(self.model).offset(skip).limit(limit)
        models = await self.db_session.execute(stmt)
        return models.unique().scalars().all()

    async def update(self, model: ModelType, update_model: Union[UpdateSchemaType, dict[str, Any]]) -> ModelType | None:
        model_data = jsonable_encoder(model)
        if isinstance(update_model, dict):
            update_data = update_model
        else:
            update_data = update_model.model_dump(exclude_unset=True)
        for field in model_data:
            if field in update_data:
                setattr(model, field, update_data[field])
        self.db_session.add(model)
        await self.db_session.commit()
        await self.db_session.refresh(model)
        return model

    async def delete(self, id: int | str | uuid.UUID) -> None:
        stmt = delete(self.model).where(self.model.id == id)
        await self.db_session.execute(stmt)
        await self.db_session.commit()

    async def clear(self, table_name: str) -> None:
        await self.db_session.execute(text(f"TRUNCATE {table_name} CASCADE;"))
        await self.db_session.commit()
