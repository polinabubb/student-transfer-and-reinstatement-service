from sqlalchemy.ext.compiler import compiles
from sqlalchemy.orm import DeclarativeBase, declared_attr
from sqlalchemy.sql.expression import ClauseElement, Executable


class Base(DeclarativeBase):
    """Base for all models."""

    __abstract__ = True

    # type_annotation_map = {
    #     enum.Enum: sqlalchemy.Enum(enum.Enum),
    # }

    repr_cols_num = 20
    repr_cols = ()

    @declared_attr.directive
    def __tablename__(cls) -> str:
        """Return the table name for the model."""
        return f"{cls.__name__.lower()}"

    def __repr__(self):
        cols = []
        for idx, col in enumerate(self.__table__.columns.keys()):
            if col in self.repr_cols or idx < self.repr_cols_num:
                cols.append(f"{col}={getattr(self, col)}")
        return f"<{self.__class__.__name__} {', '.join(cols)}>"


class explain(Executable, ClauseElement):
    def __init__(self, stmt, analyze=False):
        self.statement = stmt
        self.analyze = analyze


@compiles(explain, "postgresql")
def pg_explain(element, compiler, **kw):
    text = "EXPLAIN "
    if element.analyze:
        text += "ANALYZE "
    text += compiler.process(element.statement, **kw)

    return text
