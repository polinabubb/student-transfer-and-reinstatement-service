import enum

from sqlalchemy.orm import Mapped, mapped_column

from src.db.base import Base


class AudienceType(enum.Enum):
    FIIT = "FIIT"
    ALL = "ALL"
    EXPELLED = "EXPELLED"


class Advertize(Base):
    __tablename__ = "advertizes"

    id: Mapped[int] = mapped_column(primary_key=True)
    user: Mapped[int]  # Будет ссылаться на ID пользователя
    text: Mapped[str]
    audience: Mapped[AudienceType]
