"""Create stories

Revision ID: 8617710f5b6c
Revises: cbc1e56ac1ee
Create Date: 2024-01-17 06:20:53.654185

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = "8617710f5b6c"
down_revision: Union[str, None] = "cbc1e56ac1ee"
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "stories",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(), nullable=False),
        sa.Column("short_name", sa.String(), nullable=False),
        sa.Column("course", sa.Integer(), nullable=False),
        sa.Column("change_type", sa.Enum("PRICE", "PLACES", name="changestorytype"), nullable=False),
        sa.Column("is_price_changed", sa.Boolean(), nullable=True),
        sa.Column("old_price", sa.Integer(), nullable=True),
        sa.Column("new_price", sa.Integer(), nullable=True),
        sa.Column("is_budget_places_changed", sa.Boolean(), nullable=True),
        sa.Column("old_budget_places", sa.Integer(), nullable=True),
        sa.Column("new_budget_places", sa.Integer(), nullable=True),
        sa.Column("is_paid_places_changed", sa.Boolean(), nullable=True),
        sa.Column("old_paid_places", sa.Integer(), nullable=True),
        sa.Column("new_paid_places", sa.Integer(), nullable=True),
        sa.Column("link", sa.String(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("id"),
    )
    op.create_table(
        "tg_user_stories",
        sa.Column("story_id", sa.Integer(), nullable=False),
        sa.Column("telegram_user_id", sa.Uuid(), nullable=False),
        sa.Column("is_checked", sa.Boolean(), nullable=False),
        sa.Column("updated_at", sa.DateTime(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(["story_id"], ["stories.id"], ondelete="CASCADE"),
        sa.ForeignKeyConstraint(["telegram_user_id"], ["telegram_users.id"], ondelete="CASCADE"),
        sa.PrimaryKeyConstraint("story_id", "telegram_user_id"),
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table("tg_user_stories")
    op.drop_table("stories")
    # ### end Alembic commands ###
