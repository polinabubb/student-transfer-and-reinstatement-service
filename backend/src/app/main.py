import time
from typing import Callable

import uvicorn
from fastapi import APIRouter, FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware

from src.app.internal.checklists.transport.handlers import checklists_router
from src.app.internal.checklists_new.transport.handlers import checklists_new_router
from src.app.internal.core.auth.jwt.transport.handlers import auth_router
from src.app.internal.core.auth.tg.transport.handlers import tg_auth_router
from src.app.internal.faculties.transport.handlers import faculties_router
from src.app.internal.questions.transport.handlers import questions_router
from src.app.internal.stories.transport.handlers import stories_router
from src.app.internal.telegram_users.transport.handlers import tg_users_router
from src.app.internal.users.transport.handlers import users_router
from src.config.settings import get_settings

settings = get_settings()

app = FastAPI(
    debug=settings.DEBUG,
    title=settings.PROJECT_NAME,
    version=settings.API_VERSION,
    openapi_url=f"{settings.API_V1_STR}/openapi.json",
    docs_url="/api/docs",
    redoc_url="/api/redoc",
)

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

api_router = APIRouter(prefix=settings.API_V1_STR)

api_router.include_router(auth_router)
api_router.include_router(tg_auth_router)
api_router.include_router(users_router)
api_router.include_router(tg_users_router)
api_router.include_router(questions_router)
api_router.include_router(checklists_router)
api_router.include_router(faculties_router)
api_router.include_router(stories_router)
api_router.include_router(checklists_new_router)
app.include_router(api_router)


@app.middleware("http")
async def add_process_time_header(request: Request, call_next: Callable):
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    response.headers["X-Process-Time"] = str(process_time)
    return response


@app.get("/healthcheck", tags=["healthcheck"])
def healthcheck():
    return {"message": "Hello World"}


if __name__ == "__main__":
    uvicorn.run(app="main:app", port=settings.APP_PORT, reload=True)
