import httpx
from bs4 import BeautifulSoup

MATHMECH_COURSES_CODES = {
    "01.03.01",
    "01.03.03",
    "01.03.04",
    "02.03.01",
    "02.03.02",
    "02.03.03",
    "10.05.01",
}


def get_math_mech_vacant_places() -> dict:
    html = httpx.get(
        "https://urfu.ru/sveden/vacant/", headers={"user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)"}
    )

    soup = BeautifulSoup(html.text, "html.parser")
    table_rows = soup.find_all("tr", itemprop="vacant")
    vacant_places = {}
    for mcc in MATHMECH_COURSES_CODES:
        vacant_places[mcc] = []

    for tr in table_rows:
        course = tr.find("td", itemprop="eduCode")
        if course.text in MATHMECH_COURSES_CODES:
            edu_course = tr.find("td", itemprop="eduCourse")
            number_budget = tr.find("td", itemprop="numberBFVacant")
            number_budget_subject = tr.find("td", itemprop="numberBRVacant")
            number_budget_local = tr.find("td", itemprop="numberBMVacant")
            number_tuition = tr.find("td", itemprop="numberPVacant")

            vacant_places[course.text].append(
                {
                    "number": edu_course.text,
                    "number_budget": number_budget.text,
                    "number_budget_subject": number_budget_subject.text,
                    "number_budget_local": number_budget_local.text,
                    "number_tuition": number_tuition.text,
                    "faculty_code": course.text,
                }
            )

    return vacant_places
