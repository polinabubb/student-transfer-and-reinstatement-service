from fastapi import HTTPException

from src.app.internal.questions.dto import QuestionCreateDTO, QuestionDTO, QuestionSearchDTO
from src.app.internal.questions.repositories import QuestionRepository
from src.app.internal.questions.transport.requests import QuestionUpdateRequest


class QuestionService:
    def __init__(self, question_repo: QuestionRepository):
        self.question_repo = question_repo

    async def get_question_by_question(self, question: str) -> QuestionDTO:
        question = await self.question_repo.get_by_question(question)
        return QuestionDTO.model_validate(question)

    async def get_questions_by_question(self, question: str) -> QuestionDTO:
        questions = await self.question_repo.get_all()
        return [QuestionDTO.model_validate(question) for question in questions]

    async def get_question(self, question_id: int) -> QuestionDTO:
        question = await self.question_repo.get(question_id)
        if not question:
            raise HTTPException(status_code=404, detail="Question not found")
        return QuestionDTO.model_validate(question)

    async def get_questions(self) -> list[QuestionDTO]:
        questions = await self.question_repo.get_all()
        return [QuestionDTO.model_validate(question) for question in questions]

    async def get_multi_questions(self, *, skip: int = 0, limit: int = 100, is_frequent: bool = False):
        if is_frequent:
            questions = await self.question_repo.get_multi_frequent(skip=skip, limit=limit)
        else:
            questions = await self.question_repo.get_multi(skip=skip, limit=limit)
        return [QuestionDTO.model_validate(question) for question in questions]

    async def create(self, question: QuestionDTO) -> QuestionDTO:
        question = QuestionCreateDTO(
            question=question.question, answer=question.answer, is_frequent=question.is_frequent, count=question.count
        )
        question = await self.question_repo.create(question)
        return QuestionDTO.model_validate(question)

    async def search_trgm(self, query: str) -> list[QuestionSearchDTO]:
        questions = await self.question_repo.search_trgm(query)
        return [QuestionSearchDTO.model_validate(question) for question in questions]

    async def search_fulltext(self, query: str) -> list[QuestionSearchDTO]:
        questions = await self.question_repo.search_fulltext(query)
        return [QuestionSearchDTO.model_validate(question) for question in questions]

    async def update(self, question_id: int, question_update: QuestionUpdateRequest) -> QuestionDTO:
        question = await self.question_repo.get(question_id)
        new_question = await self.question_repo.update(question, question_update)
        return QuestionDTO.model_validate(new_question)

    async def delete(self, question_id: int) -> None:
        await self.question_repo.delete(question_id)
