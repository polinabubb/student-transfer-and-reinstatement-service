import datetime

from pydantic import BaseModel, ConfigDict


class QuestionDTO(BaseModel):
    id: int
    question: str
    answer: str
    is_frequent: bool
    count: int
    tags: list[str]
    updated_at: datetime.datetime
    created_at: datetime.datetime

    model_config = ConfigDict(from_attributes=True)


class QuestionSearchDTO(BaseModel):
    id: int
    question: str
    answer: str
    is_frequent: bool
    count: int
    tags: list[str]
    similarity_ratio: float
    updated_at: datetime.datetime
    created_at: datetime.datetime

    model_config = ConfigDict(from_attributes=True)


class QuestionCreateDTO(BaseModel):
    question: str
    answer: str
    is_frequent: bool = False
    count: int = 0


class QuestionUpdateDTO(BaseModel):
    question: str | None = None
    answer: str | None = None
    is_frequent: bool = False
    count: int = 0
