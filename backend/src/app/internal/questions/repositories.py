from sqlalchemy import func, select

from src.app.internal.questions.dto import QuestionCreateDTO, QuestionUpdateDTO
from src.app.internal.questions.models import Question, questions_search
from src.db.base_repository import BaseRepository


class QuestionRepository(BaseRepository[Question, QuestionCreateDTO, QuestionUpdateDTO]):
    model = Question

    async def get_multi_frequent(self, *, skip: int = 0, limit: int = 100) -> list[Question]:
        stmt = select(Question).where(Question.is_frequent).offset(skip).limit(limit)
        models = await self.db_session.execute(stmt)
        return models.unique().scalars().all()

    async def get_by_question(self, question: str) -> Question:
        stmt = select(Question).where(Question.question == question)
        question = await self.db_session.execute(stmt)
        return question.scalar_one_or_none()

    async def search_trgm(self, query: str) -> list[Question]:
        await questions_search.set_similarity_limit(self.db_session, 0.01)
        result = (await self.db_session.execute(questions_search(query, include_similarity_ratio=True))).all()
        return result

    async def search_fulltext(self, query: str) -> list[Question]:
        ts_rank_cd = func.ts_rank_cd(func.to_tsvector(Question.question), func.plainto_tsquery(query))
        columns = {column.table for column in [Question.question]}
        columns.add(ts_rank_cd.label("similarity_ratio"))
        stmt = (
            select(*columns)
            .where(func.to_tsvector("russian", Question.question).op("@@")(func.plainto_tsquery("russian", query)))
            .order_by(ts_rank_cd.desc())
        )
        result = await self.db_session.execute(stmt)
        return result.all()
