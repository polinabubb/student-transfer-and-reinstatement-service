import datetime

from sqlalchemy import ARRAY, BIGINT, String
from sqlalchemy.orm import Mapped, mapped_column

from src.db.base import Base
from src.db.search_service import FuzzySearchService


class Question(Base):
    __tablename__ = "questions"

    id: Mapped[int] = mapped_column(primary_key=True, unique=True)
    question: Mapped[str]
    answer: Mapped[str]
    is_frequent: Mapped[bool] = mapped_column(default=False)
    count: Mapped[BIGINT] = mapped_column(BIGINT, default=0)
    tags: Mapped[list[str]] = mapped_column(ARRAY(String), default=[])
    updated_at: Mapped[datetime.datetime] = mapped_column(
        nullable=False, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow
    )
    created_at: Mapped[datetime.datetime] = mapped_column(nullable=False, default=datetime.datetime.utcnow)


questions_search = FuzzySearchService(
    Question.question,
    Question.answer,
    similarity_limit=0.01,
    init_index=True,
)
