from pydantic import BaseModel


class QuestionResponse(BaseModel):
    id: int
    question: str
    answer: str
    is_frequent: bool
    count: int
    tags: list[str]


class QuestionSearchResponse(BaseModel):
    id: int
    question: str
    answer: str
    is_frequent: bool
    count: int
    tags: list[str]
    similarity_ratio: float
