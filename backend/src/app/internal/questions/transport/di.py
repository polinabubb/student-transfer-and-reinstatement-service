from typing import Annotated

from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from src.app.internal.questions.repositories import QuestionRepository
from src.app.internal.questions.service import QuestionService
from src.db.di import get_db


async def get_question_service(db_session: Annotated[AsyncSession, Depends(get_db)]) -> QuestionService:
    question_repo = QuestionRepository(db_session=db_session)
    return QuestionService(question_repo=question_repo)
