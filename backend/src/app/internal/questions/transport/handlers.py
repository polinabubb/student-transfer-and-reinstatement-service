from typing import Annotated

from fastapi import APIRouter, Depends

from src.app.internal.core.auth.middlewares.jwt.service import get_current_user
from src.app.internal.questions.service import QuestionService
from src.app.internal.questions.transport.di import get_question_service
from src.app.internal.questions.transport.requests import QuestionCreateRequest, QuestionUpdateRequest, SearchMethod
from src.app.internal.questions.transport.responses import QuestionResponse, QuestionSearchResponse
from src.app.internal.users.models import User

questions_router = APIRouter(prefix="/questions", tags=["questions"])


@questions_router.get(
    path="/search",
    response_model=list[QuestionSearchResponse],
    status_code=200,
)
async def search(
    query: str,
    method: SearchMethod,
    question_service: Annotated[QuestionService, Depends(get_question_service)],
) -> list[QuestionSearchResponse]:
    match method:
        case SearchMethod.TRGM:
            return await question_service.search_trgm(query)
        case SearchMethod.FULLTEXT:
            return await question_service.search_fulltext(query)


@questions_router.get(
    path="",
    response_model=list[QuestionResponse],
    status_code=200,
)
async def get_multi_questions(
    question_service: Annotated[QuestionService, Depends(get_question_service)],
    skip: int = 0,
    limit: int = 100,
    is_frequent: bool = False,
) -> list[QuestionResponse]:
    return await question_service.get_multi_questions(skip=skip, limit=limit, is_frequent=is_frequent)


@questions_router.get(
    path="/{question_id}",
    response_model=QuestionResponse,
    status_code=200,
)
async def get_question(
    question_id: int,
    question_service: Annotated[QuestionService, Depends(get_question_service)],
) -> list[QuestionResponse]:
    return await question_service.get_question(question_id)


@questions_router.post(
    path="",
    response_model=QuestionResponse,
    status_code=201,
)
async def create_question(
    question: QuestionCreateRequest,
    current_user: Annotated[User, Depends(get_current_user)],
    question_service: Annotated[QuestionService, Depends(get_question_service)],
) -> QuestionResponse:
    return await question_service.create(question)


@questions_router.patch(
    path="/{question_id}",
    response_model=QuestionResponse,
    status_code=200,
)
async def update_question(
    question_id: int,
    question: QuestionUpdateRequest,
    current_user: Annotated[User, Depends(get_current_user)],
    question_service: Annotated[QuestionService, Depends(get_question_service)],
) -> QuestionResponse:
    return await question_service.update(question_id, question)


@questions_router.delete(
    path="/{question_id}",
    status_code=204,
)
async def delete_question(
    question_id: int,
    current_user: Annotated[User, Depends(get_current_user)],
    question_service: Annotated[QuestionService, Depends(get_question_service)],
):
    await question_service.delete(question_id)
