import enum

from pydantic import BaseModel


class QuestionCreateRequest(BaseModel):
    question: str
    answer: str
    is_frequent: bool = False
    count: int = 0


class QuestionUpdateRequest(BaseModel):
    question: str | None = None
    answer: str | None = None
    is_frequent: bool = False
    count: int = 0


class SearchMethod(enum.StrEnum):
    TRGM = "trgm"
    FULLTEXT = "fulltext"
