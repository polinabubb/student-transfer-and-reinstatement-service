from fastapi import HTTPException

from src.app.internal.core.auth.dto import JWTTokenTgCreateDTO, TokensDTO
from src.app.internal.core.auth.hash import verify_data
from src.app.internal.core.auth.middlewares.auth import JWTAuth, TokenType
from src.app.internal.core.auth.middlewares.utils import try_decode_token
from src.app.internal.core.auth.repositories import JWTTokenRepository
from src.app.internal.core.auth.tg.transport.requests import WebAppInitData
from src.app.internal.core.auth.utils import generate_device_id
from src.app.internal.telegram_users.dto import TelegramUserCreateDTO, TelegramUserDTO
from src.app.internal.telegram_users.models import TelegramUser
from src.app.internal.telegram_users.repositories import TelegramUserRepository


class TelegramAuthService:
    def __init__(
        self, tg_user_repo: TelegramUserRepository, jwt_token_repo: JWTTokenRepository, jwt_auth: JWTAuth
    ) -> None:
        self.tg_user_repo = tg_user_repo
        self.jwt_token_repo = jwt_token_repo
        self.jwt_auth = jwt_auth

    async def login(self, web_app_init_data: WebAppInitData) -> TokensDTO:
        if not verify_data(web_app_init_data):
            raise HTTPException(status_code=400, detail="Incorrect data", headers={"WWW-Authenticate": "Bearer"})

        tg_user_dto = TelegramUserDTO(**web_app_init_data.user.model_dump())
        tg_user = await self.tg_user_repo.get_by_telegram_id(telegram_id=tg_user_dto.id)

        if not tg_user:
            tg_user_create = TelegramUserCreateDTO(
                telegram_id=tg_user_dto.id,
                username=tg_user_dto.username,
                first_name=tg_user_dto.first_name,
                last_name=tg_user_dto.last_name,
                language_code=tg_user_dto.language_code,
            )
            tg_user = await self.tg_user_repo.create(tg_user_create)

        return await self._issue_tokens(tg_user=tg_user)

    async def logout(self, tg_user: TelegramUser, device_id: str) -> None:
        await self.jwt_token_repo.update_by_tg_user_id_and_device_id(
            tg_user_id=tg_user.id, device_id=device_id, is_blacklisted=True
        )

    async def refresh_tokens(self, tg_user: TelegramUser, refresh_token: str) -> TokensDTO:
        payload, error = try_decode_token(jwt_auth=self.jwt_auth, token=refresh_token)

        if error:
            raise HTTPException(status_code=400, detail="Invalid token", headers={"WWW-Authenticate": "Bearer"})

        if payload.get("type") != TokenType.REFRESH.value:
            raise HTTPException(status_code=400, detail="Invalid token type", headers={"WWW-Authenticate": "Bearer"})

        if (await self.jwt_token_repo.get_by_jti(jti=payload.get("jti"))).is_blacklisted:
            await self.jwt_token_repo.update_by_tg_user_id(tg_user_id=tg_user.id, is_blacklisted=True)
            raise HTTPException(
                status_code=400, detail="This token already is blacklisted", headers={"WWW-Authenticate": "Bearer"}
            )

        device_id = payload.get("device_id")
        await self.jwt_token_repo.update_by_tg_user_id_and_device_id(
            tg_user_id=tg_user.id, device_id=device_id, is_blacklisted=True
        )

        return await self._issue_tokens(tg_user=tg_user, device_id=device_id)

    async def _issue_tokens(self, tg_user: TelegramUser, device_id: str = generate_device_id()) -> TokensDTO:
        tokens = self.jwt_auth.generate_tokens(subject=str(tg_user.id), payload={"device_id": device_id})
        raw_jwt_tokens = [self.jwt_auth.decode_token(token) for token in (tokens.access_token, tokens.refresh_token)]

        for raw_jwt_token in raw_jwt_tokens:
            jwt_token = JWTTokenTgCreateDTO(tg_user_id=tg_user.id, jti=raw_jwt_token.get("jti"), device_id=device_id)
            await self.jwt_token_repo.create(jwt_token)

        return tokens
