from pydantic import BaseModel


class RefreshTokensRequest(BaseModel):
    refresh_token: str


class WebAppUser(BaseModel):
    id: int
    is_bot: bool | None
    first_name: str | None
    last_name: str | None
    username: str | None
    language_code: str | None
    is_premium: bool | None
    added_to_attachment_menu: bool | None
    allows_write_to_pm: bool | None
    photo_url: str | None


class WebAppInitData(BaseModel):
    query_id: str
    user: WebAppUser
    auth_date: int
    hash: str
