from typing import Annotated

from fastapi import APIRouter, Depends, Request

from src.app.internal.core.auth.middlewares.tg.service import get_current_tg_user
from src.app.internal.core.auth.tg.service import TelegramAuthService
from src.app.internal.core.auth.tg.transport.di import get_tg_auth_service
from src.app.internal.core.auth.tg.transport.requests import RefreshTokensRequest, WebAppInitData
from src.app.internal.core.auth.tg.transport.responses import SuccessResponse, TokensResponse
from src.app.internal.telegram_users.models import TelegramUser

tg_auth_router = APIRouter(prefix="/telegram/auth", tags=["telegram auth"])


@tg_auth_router.post(
    path="/login",
    response_model=TokensResponse,
    status_code=200,
)
async def login_telegram(
    web_app_init_data: Annotated[WebAppInitData, Depends()],
    tg_auth_service: Annotated[TelegramAuthService, Depends(get_tg_auth_service)],
) -> TokensResponse:
    tokens = await tg_auth_service.login(web_app_init_data)
    return TokensResponse.model_validate(tokens)


@tg_auth_router.post(
    path="/refresh_tokens",
    response_model=TokensResponse,
    status_code=200,
)
async def refresh_tokens_telegram(
    body: RefreshTokensRequest,
    current_tg_user: Annotated[TelegramUser, Depends(get_current_tg_user)],
    tg_auth_service: Annotated[TelegramAuthService, Depends(get_tg_auth_service)],
) -> TokensResponse:
    tokens = await tg_auth_service.refresh_tokens(tg_user=current_tg_user, refresh_token=body.refresh_token)
    return TokensResponse.model_validate(tokens)


@tg_auth_router.post(
    path="/logout",
    response_model=SuccessResponse,
    status_code=200,
)
async def logout_telegram(
    request: Request,
    current_tg_user: Annotated[TelegramUser, Depends(get_current_tg_user)],
    tg_auth_service: Annotated[TelegramAuthService, Depends(get_tg_auth_service)],
) -> SuccessResponse:
    await tg_auth_service.logout(tg_user=current_tg_user, device_id=request.state.device_id)
    return SuccessResponse()
