from typing import Annotated, Optional

from fastapi import Depends, HTTPException
from fastapi.security.utils import get_authorization_scheme_param
from jose import jwt
from sqlalchemy.ext.asyncio import AsyncSession
from starlette.requests import Request
from starlette.status import HTTP_401_UNAUTHORIZED

from src.app.internal.core.auth.middlewares.auth import TokenType
from src.app.internal.core.auth.models import JWTToken
from src.app.internal.telegram_users.models import TelegramUser
from src.config.settings import get_settings
from src.db.di import get_db


class OAuth2TelegramBearer:
    def __init__(self, token_url: str, auto_error: bool = True):
        self.token_url = token_url
        self.auto_error = auto_error

    async def __call__(self, request: Request) -> Optional[str]:
        authorization = request.headers.get("Authorization")
        scheme, param = get_authorization_scheme_param(authorization)
        if not authorization or scheme.lower() != "bearer":
            if self.auto_error:
                raise HTTPException(
                    status_code=HTTP_401_UNAUTHORIZED,
                    detail="Not authenticated",
                    headers={"WWW-Authenticate": "Bearer"},
                )
            else:
                return None
        return param


oauth2_tg_scheme = OAuth2TelegramBearer(token_url=f"{get_settings().API_V1_STR}/auth/telegram/login", auto_error=False)


async def get_current_tg_user(
    request: Request,
    token: Annotated[str, Depends(oauth2_tg_scheme)],
    db: Annotated[AsyncSession, Depends(get_db)],
) -> TelegramUser:
    if not token:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail="Not authenticated",
            headers={"WWW-Authenticate": "Bearer"},
        )
    try:
        payload = jwt.decode(
            token, get_settings().jwt_config.secret_key, algorithms=[get_settings().jwt_config.algorithm]
        )
        if payload.get("type") != TokenType.ACCESS.value:
            raise HTTPException(status_code=403, detail="The passed token does not match the required type")
    except jwt.JWTError:
        raise HTTPException(status_code=403, detail="The transferred token is invalid")

    if (await db.get(JWTToken, payload.get("jti"))).is_blacklisted:
        raise HTTPException(status_code=403, detail="The transferred token is blacklisted")

    tg_user = await db.get(TelegramUser, payload.get("sub"))
    if not tg_user:
        raise HTTPException(status_code=403, detail="The owner of this access token has not been found")
    request.state.device_id = payload.get("device_id")
    return tg_user


async def get_current_tg_user_or_none(
    request: Request,
    token: Annotated[str | None, Depends(oauth2_tg_scheme)],
    db: Annotated[AsyncSession, Depends(get_db)],
) -> TelegramUser | None:
    if not token:
        return None
    try:
        payload = jwt.decode(
            token, get_settings().jwt_config.secret_key, algorithms=[get_settings().jwt_config.algorithm]
        )
        if payload.get("type") != TokenType.ACCESS.value:
            raise HTTPException(status_code=403, detail="The passed token does not match the required type")
    except jwt.JWTError:
        raise HTTPException(status_code=403, detail="The transferred token is invalid")

    if (await db.get(JWTToken, payload.get("jti"))).is_blacklisted:
        raise HTTPException(status_code=403, detail="The transferred token is blacklisted")

    tg_user = await db.get(TelegramUser, payload.get("sub"))
    if not tg_user:
        raise HTTPException(status_code=403, detail="The owner of this access token has not been found")
    request.state.device_id = payload.get("device_id")
    return tg_user
