import uuid

from sqlalchemy import Boolean, ForeignKey, String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.db.base import Base


class JWTToken(Base):
    __tablename__ = "jwt_tokens"

    jti: Mapped[str] = mapped_column(String(length=36), primary_key=True, unique=True, index=True, nullable=False)
    is_blacklisted: Mapped[bool] = mapped_column(Boolean, default=False, nullable=False)
    user_id: Mapped[uuid.UUID] = mapped_column(ForeignKey("users.id"), nullable=True)
    user: Mapped["User"] = relationship(back_populates="jwt_tokens")  # noqa: F821
    device_id: Mapped[str] = mapped_column(String(length=36), nullable=False)
    tg_user_id: Mapped[uuid.UUID] = mapped_column(ForeignKey("telegram_users.id"), nullable=True)
    tg_user: Mapped["TelegramUser"] = relationship(back_populates="jwt_tokens")  # noqa: F821
