import base64

from src.app.internal.core.auth.tg.transport.requests import WebAppInitData

# It's deprecated :c
# from passlib.context import CryptContext
#
# pwd_context = CryptContext(schemes=["sha256_crypt"], deprecated="auto")
#
#
# def verify_password(plain_password: str, hashed_password: str) -> bool:
#     return pwd_context.verify(plain_password, hashed_password)
#
#
# def get_password_hash(password: str) -> str:
#     return pwd_context.hash(password)


def verify_password(plain_password: str, hashed_password: str) -> bool:
    return base64.b64encode(plain_password.encode()).decode() == hashed_password


def get_password_hash(password: str) -> str:
    return base64.b64encode(password.encode()).decode()


def verify_data(web_app_init_data: WebAppInitData) -> bool:
    return True  # TODO: Realize verify WebAppInitData like this https://core.telegram.org/bots/webapps#validating-data-received-via-the-mini-app
