import uuid

from sqlalchemy import and_, select, update

from src.app.internal.core.auth.dto import JWTTokenCreateDTO, JWTTokenUpdateDTO
from src.app.internal.core.auth.models import JWTToken
from src.db.base_repository import BaseRepository


class JWTTokenRepository(BaseRepository[JWTToken, JWTTokenCreateDTO, JWTTokenUpdateDTO]):
    model = JWTToken

    async def get_by_user_id(self, user_id: uuid.UUID) -> list[JWTToken]:
        stmt = select(JWTToken).where(JWTToken.user_id == user_id)
        jwt_tokens = await self.db_session.execute(stmt)
        return jwt_tokens.scalars().all()

    async def get_by_tg_user_id(self, tg_user_id: uuid.UUID) -> list[JWTToken]:
        stmt = select(JWTToken).where(JWTToken.tg_user_id == tg_user_id)
        jwt_tokens = await self.db_session.execute(stmt)
        return jwt_tokens.scalars().all()

    async def get_by_jti(self, jti: str) -> JWTToken | None:
        jwt_token = await self.db_session.get(JWTToken, jti)
        return jwt_token

    async def update(self, jti: str, **kwargs) -> JWTToken | None:
        stmt = update(JWTToken).where(JWTToken.jti == jti).values(**kwargs).returning(JWTToken)
        jwt_token = await self.db_session.execute(stmt)
        await self.db_session.commit()
        return jwt_token.scalar_one_or_none()

    async def update_by_user_id(self, user_id: uuid.UUID, **kwargs) -> None:
        stmt = update(JWTToken).where(JWTToken.user_id == user_id).values(**kwargs)
        await self.db_session.execute(stmt)
        await self.db_session.commit()

    async def update_by_tg_user_id(self, tg_user_id: uuid.UUID, **kwargs) -> None:
        stmt = update(JWTToken).where(JWTToken.tg_user_id == tg_user_id).values(**kwargs)
        await self.db_session.execute(stmt)
        await self.db_session.commit()

    async def update_by_user_id_and_device_id(self, user_id: uuid.UUID, device_id: str, **kwargs) -> None:
        stmt = (
            update(JWTToken)
            .where(and_((JWTToken.user_id == user_id), (JWTToken.device_id == device_id)))
            .values(**kwargs)
        )
        await self.db_session.execute(stmt)
        await self.db_session.commit()

    async def update_by_tg_user_id_and_device_id(self, tg_user_id: uuid.UUID, device_id: str, **kwargs) -> None:
        stmt = (
            update(JWTToken)
            .where(and_((JWTToken.tg_user_id == tg_user_id), (JWTToken.device_id == device_id)))
            .values(**kwargs)
        )
        await self.db_session.execute(stmt)
        await self.db_session.commit()
