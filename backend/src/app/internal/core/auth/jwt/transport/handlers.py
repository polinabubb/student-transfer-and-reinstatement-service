from typing import Annotated

from fastapi import APIRouter, Depends, Request
from fastapi.security import OAuth2PasswordRequestForm

from src.app.internal.core.auth.jwt.service import AuthService
from src.app.internal.core.auth.jwt.transport.di import get_auth_service
from src.app.internal.core.auth.jwt.transport.requests import RefreshTokensRequest
from src.app.internal.core.auth.jwt.transport.responses import SuccessResponse, TokensResponse
from src.app.internal.core.auth.middlewares.jwt.service import get_current_user
from src.app.internal.users.dto import UserDTO
from src.app.internal.users.models import User

auth_router = APIRouter(prefix="/auth", tags=["auth"])


@auth_router.post(
    path="/register",
    response_model=TokensResponse,
    status_code=201,
)
async def register(
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()],
    auth_service: Annotated[AuthService, Depends(get_auth_service)],
) -> TokensResponse:
    user_dto = UserDTO(email=form_data.username, password=form_data.password)
    tokens = await auth_service.register(user_dto)
    return TokensResponse.model_validate(tokens)


@auth_router.post(
    path="/login",
    response_model=TokensResponse,
    status_code=200,
)
async def login(
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()],
    auth_service: Annotated[AuthService, Depends(get_auth_service)],
) -> TokensResponse:
    user_dto = UserDTO(email=form_data.username, password=form_data.password)
    tokens = await auth_service.login(user_dto)
    return TokensResponse.model_validate(tokens)


@auth_router.post(
    path="/logout",
    response_model=SuccessResponse,
    status_code=200,
)
async def logout(
    request: Request,
    current_user: Annotated[User, Depends(get_current_user)],
    auth_service: Annotated[AuthService, Depends(get_auth_service)],
) -> SuccessResponse:
    await auth_service.logout(user=current_user, device_id=request.state.device_id)
    return SuccessResponse()


@auth_router.post(
    path="/refresh_tokens",
    response_model=TokensResponse,
    status_code=200,
)
async def refresh_tokens(
    body: RefreshTokensRequest,
    current_user: Annotated[User, Depends(get_current_user)],
    auth_service: Annotated[AuthService, Depends(get_auth_service)],
) -> TokensResponse:
    tokens = await auth_service.refresh_tokens(user=current_user, refresh_token=body.refresh_token)
    return TokensResponse.model_validate(tokens)
