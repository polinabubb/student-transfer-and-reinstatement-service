import uuid

from pydantic import BaseModel


class TokensDTO(BaseModel):
    access_token: str
    refresh_token: str


class JWTTokenCreateDTO(BaseModel):
    user_id: uuid.UUID
    jti: str
    device_id: str


class JWTTokenUpdateDTO(BaseModel):
    user_id: uuid.UUID | None = None
    jti: str | None = None
    device_id: str | None = None


class JWTTokenTgCreateDTO(BaseModel):
    tg_user_id: uuid.UUID
    jti: str
    device_id: str
