import datetime
import enum
import uuid

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column

from src.db.base import Base


class ChangeStoryType(enum.Enum):
    PRICE = "PRICE"
    PLACES = "PLACES"


class Story(Base):
    __tablename__ = "stories"

    id: Mapped[int] = mapped_column(primary_key=True, unique=True)
    name: Mapped[str]
    short_name: Mapped[str]
    course: Mapped[int]
    change_type: Mapped[ChangeStoryType]

    is_price_changed: Mapped[bool] = mapped_column(default=False, nullable=True)
    old_price: Mapped[int] = mapped_column(nullable=True)
    new_price: Mapped[int] = mapped_column(nullable=True)

    is_budget_places_changed: Mapped[bool] = mapped_column(default=False, nullable=True)
    old_budget_places: Mapped[int] = mapped_column(nullable=True)
    new_budget_places: Mapped[int] = mapped_column(nullable=True)

    is_paid_places_changed: Mapped[bool] = mapped_column(default=False, nullable=True)
    old_paid_places: Mapped[int] = mapped_column(nullable=True)
    new_paid_places: Mapped[int] = mapped_column(nullable=True)

    link: Mapped[str] = mapped_column(nullable=True)

    updated_at: Mapped[datetime.datetime] = mapped_column(
        nullable=False, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow
    )
    created_at: Mapped[datetime.datetime] = mapped_column(nullable=False, default=datetime.datetime.utcnow)


class TgUserStory(Base):
    __tablename__ = "tg_user_stories"

    story_id: Mapped[int] = mapped_column(
        ForeignKey("stories.id", ondelete="CASCADE"), primary_key=True, nullable=False
    )
    telegram_user_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("telegram_users.id", ondelete="CASCADE"), primary_key=True, nullable=False
    )
    is_checked: Mapped[bool] = mapped_column(default=False, nullable=False)

    updated_at: Mapped[datetime.datetime] = mapped_column(
        nullable=False, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow
    )
    created_at: Mapped[datetime.datetime] = mapped_column(nullable=False, default=datetime.datetime.utcnow)
