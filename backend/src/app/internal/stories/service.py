import uuid

from fastapi import HTTPException

from src.app.internal.stories.dto import StoryCreateDTO, StoryDTO, StoryUpdateDTO, TgUserStoryDTO
from src.app.internal.stories.repositories import StoryRepository


class StoryService:
    def __init__(self, story_repo: StoryRepository):
        self.story_repo = story_repo

    async def get_stories(self) -> list[StoryDTO]:
        stories = await self.story_repo.get_all()
        return [StoryDTO.model_validate(story) for story in stories]

    async def create(self, story: StoryCreateDTO) -> StoryDTO:
        story = await self.story_repo.create(story)
        return StoryDTO.model_validate(story)

    async def get_story(self, story_id: int) -> StoryDTO:
        story = await self.story_repo.get(story_id)
        if not story:
            raise HTTPException(status_code=404, detail="Story not found")
        return StoryDTO.model_validate(story)

    async def get_multi_stories(self, *, skip: int = 0, limit: int = 100) -> list[StoryDTO]:
        stories = await self.story_repo.get_multi(skip=skip, limit=limit)
        return [StoryDTO.model_validate(story) for story in stories]

    async def update(self, story_id: int, story_update: StoryUpdateDTO) -> StoryDTO:
        story = await self.story_repo.get(story_id)
        if not story:
            raise HTTPException(status_code=404, detail="Story not found")
        new_story = await self.story_repo.update(story, story_update)
        return StoryDTO.model_validate(new_story)

    async def delete(self, story_id: int) -> None:
        await self.story_repo.delete(story_id)

    async def get_stories_by_tg_user_id(self, telegram_user_id: uuid.UUID) -> list[TgUserStoryDTO]:
        stories = await self.story_repo.get_stories_by_tg_user_id(telegram_user_id)
        return [TgUserStoryDTO.model_validate(story) for story in stories]

    async def set_is_checked_story(
        self, story_id: int, telegram_user_id: uuid.UUID, is_checked: bool
    ) -> TgUserStoryDTO:
        story = await self.story_repo.get(story_id)
        if not story:
            raise HTTPException(status_code=404, detail="Story not found")
        new_story = await self.story_repo.set_is_checked_story(story_id, telegram_user_id, is_checked)
        return TgUserStoryDTO.model_validate(new_story)
