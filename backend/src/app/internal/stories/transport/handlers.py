from typing import Annotated

from fastapi import APIRouter, Depends

from src.app.internal.core.auth.middlewares.jwt.service import get_current_user
from src.app.internal.core.auth.middlewares.tg.service import get_current_tg_user
from src.app.internal.stories.service import StoryService
from src.app.internal.stories.transport.di import get_story_service
from src.app.internal.stories.transport.requests import StoryCreateRequest, StoryUpdateRequest
from src.app.internal.stories.transport.responses import StoryResponse, TgUserStoryResponse
from src.app.internal.telegram_users.models import TelegramUser
from src.app.internal.users.models import User

stories_router = APIRouter(prefix="/stories", tags=["stories"])


@stories_router.get(
    path="",
    response_model=list[StoryResponse],
    status_code=200,
)
async def get_multi_stories(
    story_service: Annotated[StoryService, Depends(get_story_service)],
    skip: int = 0,
    limit: int = 100,
) -> list[StoryResponse]:
    return await story_service.get_multi_stories(skip=skip, limit=limit)


@stories_router.get(
    path="/check",
    response_model=list[TgUserStoryResponse],
    status_code=200,
)
async def get_stories_by_tg_user_id(
    current_tg_user: Annotated[TelegramUser, Depends(get_current_tg_user)],
    story_service: Annotated[StoryService, Depends(get_story_service)],
) -> list[TgUserStoryResponse]:
    return await story_service.get_stories_by_tg_user_id(current_tg_user.id)


@stories_router.post(
    path="/check/{story_id}",
    response_model=TgUserStoryResponse,
    status_code=201,
)
async def set_is_checked_story(
    story_id: int,
    is_checked: bool,
    current_tg_user: Annotated[TelegramUser, Depends(get_current_tg_user)],
    story_service: Annotated[StoryService, Depends(get_story_service)],
) -> TgUserStoryResponse:
    return await story_service.set_is_checked_story(story_id, current_tg_user.id, is_checked)


@stories_router.get(
    path="/{story_id}",
    response_model=StoryResponse,
    status_code=200,
)
async def get_story(
    story_id: int,
    story_service: Annotated[StoryService, Depends(get_story_service)],
) -> StoryResponse:
    return await story_service.get_story(story_id)


@stories_router.post(
    path="",
    response_model=StoryResponse,
    status_code=201,
)
async def create_story(
    story: StoryCreateRequest,
    current_user: Annotated[User, Depends(get_current_user)],
    story_service: Annotated[StoryService, Depends(get_story_service)],
) -> StoryResponse:
    return await story_service.create(story)


@stories_router.patch(
    path="/{story_id}",
    response_model=StoryResponse,
    status_code=200,
)
async def update_story(
    story_id: int,
    story: StoryUpdateRequest,
    current_user: Annotated[User, Depends(get_current_user)],
    story_service: Annotated[StoryService, Depends(get_story_service)],
) -> StoryResponse:
    return await story_service.update(story_id, story)


@stories_router.delete(
    path="/{story_id}",
    status_code=204,
)
async def delete_story(
    story_id: int,
    current_user: Annotated[User, Depends(get_current_user)],
    story_service: Annotated[StoryService, Depends(get_story_service)],
):
    await story_service.delete(story_id)
