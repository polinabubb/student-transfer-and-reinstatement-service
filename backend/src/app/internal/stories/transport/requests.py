from typing import Optional

from pydantic import BaseModel

from src.app.internal.stories.models import ChangeStoryType


class StoryCreateRequest(BaseModel):
    name: str
    short_name: str
    course: int
    change_type: ChangeStoryType

    is_price_changed: Optional[bool] = False
    old_price: Optional[int]
    new_price: Optional[int]

    is_budget_places_changed: Optional[bool] = False
    old_budget_places: Optional[int]
    new_budget_places: Optional[int]

    is_paid_places_changed: Optional[bool] = False
    old_paid_places: Optional[int]
    new_paid_places: Optional[int]

    link: Optional[str]


class StoryUpdateRequest(BaseModel):
    name: Optional[str]
    short_name: Optional[str]
    course: Optional[int]
    change_type: Optional[ChangeStoryType]

    is_price_changed: Optional[bool] = False
    old_price: Optional[int]
    new_price: Optional[int]

    is_budget_places_changed: Optional[bool] = False
    old_budget_places: Optional[int]
    new_budget_places: Optional[int]

    is_paid_places_changed: Optional[bool] = False
    old_paid_places: Optional[int]
    new_paid_places: Optional[int]

    link: Optional[str]
