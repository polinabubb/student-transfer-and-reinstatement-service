from typing import Annotated

from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from src.app.internal.stories.repositories import StoryRepository
from src.app.internal.stories.service import StoryService
from src.db.di import get_db


async def get_story_service(db_session: Annotated[AsyncSession, Depends(get_db)]) -> StoryService:
    story_repo = StoryRepository(db_session=db_session)
    return StoryService(story_repo=story_repo)
