import datetime
from typing import Optional

from pydantic import BaseModel

from src.app.internal.stories.models import ChangeStoryType


class StoryResponse(BaseModel):
    id: int
    name: str
    short_name: str
    course: int
    change_type: ChangeStoryType

    is_price_changed: Optional[bool] = False
    old_price: Optional[int]
    new_price: Optional[int]

    is_budget_places_changed: Optional[bool] = False
    old_budget_places: Optional[int]
    new_budget_places: Optional[int]

    is_paid_places_changed: Optional[bool] = False
    old_paid_places: Optional[int]
    new_paid_places: Optional[int]

    link: Optional[str]

    updated_at: datetime.datetime
    created_at: datetime.datetime


class TgUserStoryResponse(BaseModel):
    story_id: int
    is_checked: bool
    updated_at: datetime.datetime
    created_at: datetime.datetime
