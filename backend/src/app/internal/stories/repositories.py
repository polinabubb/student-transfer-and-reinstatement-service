import uuid

from sqlalchemy import select
from sqlalchemy.dialects.postgresql import insert

from src.app.internal.stories.dto import StoryCreateDTO, StoryUpdateDTO
from src.app.internal.stories.models import Story, TgUserStory
from src.db.base_repository import BaseRepository


class StoryRepository(BaseRepository[Story, StoryCreateDTO, StoryUpdateDTO]):
    model = Story

    async def get_stories_by_tg_user_id(self, telegram_user_id: uuid.UUID) -> list[TgUserStory]:
        stmt = select(TgUserStory).where(TgUserStory.telegram_user_id == telegram_user_id)
        stories = await self.db_session.execute(stmt)
        return stories.scalars().all()

    async def set_is_checked_story(self, story_id: int, telegram_user_id: uuid.UUID, is_checked: bool) -> TgUserStory:
        stmt = insert(TgUserStory).values(story_id=story_id, telegram_user_id=telegram_user_id, is_checked=is_checked)
        on_update_stmt = stmt.on_conflict_do_update(
            index_elements=["story_id", "telegram_user_id"], set_={"is_checked": is_checked}
        ).returning(TgUserStory)
        result = await self.db_session.execute(on_update_stmt)
        await self.db_session.commit()
        return result.scalar_one_or_none()
