from sqlalchemy import select

from src.app.internal.telegram_users.dto import TelegramUserCreateDTO, TelegramUserUpdateDTO
from src.app.internal.telegram_users.models import TelegramUser
from src.db.base_repository import BaseRepository


class TelegramUserRepository(BaseRepository[TelegramUser, TelegramUserCreateDTO, TelegramUserUpdateDTO]):
    model = TelegramUser

    async def get_by_telegram_id(self, telegram_id: int) -> TelegramUser:
        stmt = select(TelegramUser).where(TelegramUser.telegram_id == telegram_id)
        telegram_user = await self.db_session.execute(stmt)
        return telegram_user.scalar_one_or_none()
