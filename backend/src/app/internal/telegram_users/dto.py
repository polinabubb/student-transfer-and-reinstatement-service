import datetime
import uuid

from pydantic import BaseModel, ConfigDict


class TelegramUserCreateDTO(BaseModel):
    telegram_id: int
    username: str | None
    first_name: str | None
    last_name: str | None
    language_code: str | None


class TelegramUserUpdateDTO(BaseModel):
    telegram_id: int | None = None
    username: int | None = None
    first_name: int | None = None
    last_name: int | None = None
    language_code: int | None = None


class TelegramUserDTO(BaseModel):
    id: int
    username: str | None
    first_name: str | None
    last_name: str | None
    language_code: str | None


class TelegramUserProfileDTO(BaseModel):
    id: uuid.UUID
    telegram_id: int
    username: str | None
    first_name: str | None
    last_name: str | None
    language_code: str | None
    is_active: bool
    is_superuser: bool
    updated_at: datetime.datetime
    created_at: datetime.datetime

    model_config = ConfigDict(from_attributes=True)
