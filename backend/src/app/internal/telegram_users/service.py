import uuid

from src.app.internal.telegram_users.dto import TelegramUserProfileDTO
from src.app.internal.telegram_users.models import TelegramUser
from src.app.internal.telegram_users.repositories import TelegramUserRepository


class TelegramUserService:
    def __init__(self, tg_user_repo: TelegramUserRepository):
        self.tg_user_repo = tg_user_repo

    def get_me(self, tg_user: TelegramUser) -> TelegramUserProfileDTO:
        return TelegramUserProfileDTO.model_validate(tg_user)

    async def get_user(self, user_id: str) -> TelegramUserProfileDTO:
        tg_user = await self.tg_user_repo.get(uuid.UUID(user_id))
        return TelegramUserProfileDTO.model_validate(tg_user)

    async def get_users(self) -> list[TelegramUserProfileDTO]:
        tg_users = await self.tg_user_repo.get_all()
        return [TelegramUserProfileDTO.model_validate(tg_user) for tg_user in tg_users]
