import datetime
import uuid

from pydantic import BaseModel


class TelegramUserProfileResponse(BaseModel):
    id: uuid.UUID
    telegram_id: int
    username: str | None
    first_name: str | None
    last_name: str | None
    language_code: str | None
    is_active: bool
    is_superuser: bool
    updated_at: datetime.datetime
    created_at: datetime.datetime
