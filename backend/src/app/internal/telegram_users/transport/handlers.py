from typing import Annotated

from fastapi import APIRouter, Depends

from src.app.internal.core.auth.middlewares.tg.service import get_current_tg_user
from src.app.internal.telegram_users.models import TelegramUser
from src.app.internal.telegram_users.service import TelegramUserService
from src.app.internal.telegram_users.transport.di import get_tg_user_service
from src.app.internal.telegram_users.transport.responses import TelegramUserProfileResponse

tg_users_router = APIRouter(prefix="/telegram/users", tags=["telegram users"])


@tg_users_router.get(
    path="/me",
    response_model=TelegramUserProfileResponse,
    status_code=200,
)
async def get_me(
    current_tg_user: Annotated[TelegramUser, Depends(get_current_tg_user)],
    tg_user_service: Annotated[TelegramUserService, Depends(get_tg_user_service)],
) -> TelegramUserProfileResponse:
    return tg_user_service.get_me(current_tg_user)


@tg_users_router.get(
    path="/{tg_user_id}",
    response_model=TelegramUserProfileResponse,
    status_code=200,
)
async def get_telegram_user(
    tg_user_id: str,
    tg_user_service: Annotated[TelegramUserService, Depends(get_tg_user_service)],
) -> TelegramUserProfileResponse:
    return await tg_user_service.get_user(tg_user_id)


@tg_users_router.get(
    path="",
    response_model=list[TelegramUserProfileResponse],
    status_code=200,
)
async def get_telegram_users(
    tg_users_service: Annotated[TelegramUserService, Depends(get_tg_user_service)],
) -> list[TelegramUserProfileResponse]:
    return await tg_users_service.get_users()
