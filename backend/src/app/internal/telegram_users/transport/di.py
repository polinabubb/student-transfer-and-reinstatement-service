from typing import Annotated

from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from src.app.internal.telegram_users.repositories import TelegramUserRepository
from src.app.internal.telegram_users.service import TelegramUserService
from src.db.di import get_db


async def get_tg_user_service(db_session: Annotated[AsyncSession, Depends(get_db)]) -> TelegramUserService:
    tg_user_repo = TelegramUserRepository(db_session=db_session)
    return TelegramUserService(tg_user_repo=tg_user_repo)
