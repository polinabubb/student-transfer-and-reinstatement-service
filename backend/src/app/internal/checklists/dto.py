import datetime

from pydantic import BaseModel, ConfigDict

from src.app.internal.checklists.models import ChecklistType


class ChecklistPointDTO(BaseModel):
    id: int
    text: str
    updated_at: datetime.datetime
    created_at: datetime.datetime

    model_config = ConfigDict(from_attributes=True)


class ChecklistDTO(BaseModel):
    id: int
    title: str
    description: str
    type: ChecklistType
    checklists_points: list[ChecklistPointDTO]
    updated_at: datetime.datetime
    created_at: datetime.datetime

    model_config = ConfigDict(from_attributes=True)


class ChecklistOnlyDTO(BaseModel):
    id: int
    title: str
    description: str
    type: ChecklistType
    updated_at: datetime.datetime
    created_at: datetime.datetime

    model_config = ConfigDict(from_attributes=True)


class ChecklistPointCreateDTO(BaseModel):
    text: str


class ChecklistCreateDTO(BaseModel):
    title: str
    description: str
    type: ChecklistType
    checklists_points: list[ChecklistPointCreateDTO]


class ChecklistPointUpdateDTO(BaseModel):
    id: int
    text: str | None = None


class ChecklistUpdateDTO(BaseModel):
    title: str | None = None
    description: str | None = None
    type: ChecklistType | None = None
    checklists_points: list[ChecklistPointUpdateDTO] | None = []


class TgUserChecklistsPointsDTO(BaseModel):
    checklists_point_id: int
    is_checked: bool
    updated_at: datetime.datetime
    created_at: datetime.datetime

    model_config = ConfigDict(from_attributes=True)
