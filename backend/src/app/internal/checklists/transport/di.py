from typing import Annotated

from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from src.app.internal.checklists.repositories import ChecklistRepository
from src.app.internal.checklists.service import ChecklistService
from src.db.di import get_db


async def get_checklist_service(db_session: Annotated[AsyncSession, Depends(get_db)]) -> ChecklistService:
    checklist_repo = ChecklistRepository(db_session=db_session)
    return ChecklistService(checklist_repo=checklist_repo)
