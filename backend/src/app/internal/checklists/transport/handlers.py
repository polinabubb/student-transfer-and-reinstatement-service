from typing import Annotated

from fastapi import APIRouter, Depends

from src.app.internal.checklists.service import ChecklistService
from src.app.internal.checklists.transport.di import get_checklist_service
from src.app.internal.checklists.transport.requests import ChecklistCreateRequest, ChecklistUpdateRequest
from src.app.internal.checklists.transport.responses import ChecklistResponse, TgUserChecklistsPointsResponse
from src.app.internal.core.auth.middlewares.jwt.service import get_current_user
from src.app.internal.core.auth.middlewares.tg.service import get_current_tg_user
from src.app.internal.telegram_users.models import TelegramUser
from src.app.internal.users.models import User

checklists_router = APIRouter(prefix="/checklists", tags=["checklists"])


@checklists_router.get(
    path="",
    response_model=list[ChecklistResponse],
    status_code=200,
)
async def get_multi_checklists(
    checklist_service: Annotated[ChecklistService, Depends(get_checklist_service)],
    skip: int = 0,
    limit: int = 100,
) -> list[ChecklistResponse]:
    return await checklist_service.get_multi_checklists(skip=skip, limit=limit)


@checklists_router.get(
    path="/check",
    response_model=list[TgUserChecklistsPointsResponse],
    status_code=200,
)
async def get_checklist_points_by_tg_user_id(
    current_tg_user: Annotated[TelegramUser, Depends(get_current_tg_user)],
    checklist_service: Annotated[ChecklistService, Depends(get_checklist_service)],
) -> list[TgUserChecklistsPointsResponse]:
    return await checklist_service.get_checklist_points_by_tg_user_id(current_tg_user.id)


@checklists_router.post(
    path="/check/{checklist_point_id}",
    response_model=TgUserChecklistsPointsResponse,
    status_code=201,
)
async def set_is_checked_checklist_point(
    checklist_point_id: int,
    is_checked: bool,
    current_tg_user: Annotated[TelegramUser, Depends(get_current_tg_user)],
    checklist_service: Annotated[ChecklistService, Depends(get_checklist_service)],
) -> TgUserChecklistsPointsResponse:
    return await checklist_service.set_is_checked_checklist_point(checklist_point_id, current_tg_user.id, is_checked)


@checklists_router.get(
    path="/{checklist_id}",
    response_model=ChecklistResponse,
    status_code=200,
)
async def get_checklist(
    checklist_id: int,
    checklist_service: Annotated[ChecklistService, Depends(get_checklist_service)],
) -> ChecklistResponse:
    return await checklist_service.get_checklist(checklist_id)


@checklists_router.post(
    path="",
    response_model=ChecklistResponse,
    status_code=201,
)
async def create_checklist(
    checklist: ChecklistCreateRequest,
    current_user: Annotated[User, Depends(get_current_user)],
    checklist_service: Annotated[ChecklistService, Depends(get_checklist_service)],
) -> ChecklistResponse:
    return await checklist_service.create(checklist)


@checklists_router.patch(
    path="/{checklist_id}",
    response_model=ChecklistResponse,
    status_code=200,
)
async def update_checklist(
    checklist_id: int,
    checklist: ChecklistUpdateRequest,
    current_user: Annotated[User, Depends(get_current_user)],
    checklist_service: Annotated[ChecklistService, Depends(get_checklist_service)],
) -> ChecklistResponse:
    return await checklist_service.update(checklist_id, checklist)


@checklists_router.delete(
    path="/{checklist_id}",
    status_code=204,
)
async def delete_checklist(
    checklist_id: int,
    current_user: Annotated[User, Depends(get_current_user)],
    checklist_service: Annotated[ChecklistService, Depends(get_checklist_service)],
):
    await checklist_service.delete(checklist_id)
