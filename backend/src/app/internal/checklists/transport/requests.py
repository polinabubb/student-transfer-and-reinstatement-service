from pydantic import BaseModel

from src.app.internal.checklists.models import ChecklistType


class ChecklistPointCreateRequest(BaseModel):
    text: str


class ChecklistCreateRequest(BaseModel):
    title: str
    description: str
    type: ChecklistType
    checklists_points: list[ChecklistPointCreateRequest]


class ChecklistPointUpdateRequest(BaseModel):
    id: int
    text: str | None = None


class ChecklistUpdateRequest(BaseModel):
    title: str | None = None
    description: str | None = None
    type: ChecklistType | None = None
    checklists_points: list[ChecklistPointUpdateRequest] | None = []
