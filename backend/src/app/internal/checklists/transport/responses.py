import datetime

from pydantic import BaseModel

from src.app.internal.checklists.models import ChecklistType


class ChecklistPointResponse(BaseModel):
    id: int
    text: str
    updated_at: datetime.datetime
    created_at: datetime.datetime


class ChecklistResponse(BaseModel):
    id: int
    title: str
    description: str
    type: ChecklistType
    checklists_points: list[ChecklistPointResponse]
    updated_at: datetime.datetime
    created_at: datetime.datetime


class TgUserChecklistsPointsResponse(BaseModel):
    checklists_point_id: int
    is_checked: bool
    updated_at: datetime.datetime
    created_at: datetime.datetime
