import uuid
from typing import Any, Union

from sqlalchemy import select
from sqlalchemy.dialects.postgresql import insert

from src.app.internal.checklists.dto import ChecklistCreateDTO, ChecklistPointUpdateDTO, ChecklistUpdateDTO
from src.app.internal.checklists.models import Checklist, ChecklistPoint, TgUserChecklistsPoints
from src.db.base_repository import BaseRepository


class ChecklistRepository(BaseRepository[Checklist, ChecklistCreateDTO, ChecklistUpdateDTO]):
    model = Checklist

    async def create(self, checklist: ChecklistCreateDTO) -> Checklist:
        new_checklist = Checklist(
            title=checklist.title,
            description=checklist.description,
            type=checklist.type,
        )
        for point in checklist.checklists_points:
            checklist_point = ChecklistPoint(text=point.text)
            new_checklist.checklists_points.append(checklist_point)
        self.db_session.add(new_checklist)
        await self.db_session.commit()
        await self.db_session.refresh(new_checklist)
        return new_checklist

    async def update(
        self, model: Checklist, update_model: Union[ChecklistUpdateDTO, dict[str, Any]]
    ) -> Checklist | None:
        if isinstance(update_model, dict):
            update_data = update_model
        else:
            update_data = update_model.model_dump(exclude_unset=True)
        if update_data.get("checklists_points", None):
            del update_data["checklists_points"]
        return await super().update(model, update_data)

    async def get_point(self, id: int | str | uuid.UUID) -> ChecklistPoint:
        db_model = await self.db_session.get(ChecklistPoint, id)
        return db_model

    async def update_checklist_points(
        self, model: ChecklistPoint, update_model: Union[ChecklistPointUpdateDTO, dict[str, Any]]
    ) -> ChecklistPoint | None:
        return await super().update(model, update_model)

    async def set_is_checked_checklist_point(
        self, checklist_point_id: int, telegram_user_id: uuid.UUID, is_checked: bool
    ) -> TgUserChecklistsPoints:
        stmt = insert(TgUserChecklistsPoints).values(
            checklists_point_id=checklist_point_id, telegram_user_id=telegram_user_id, is_checked=is_checked
        )
        on_update_stmt = stmt.on_conflict_do_update(
            index_elements=["checklists_point_id", "telegram_user_id"], set_={"is_checked": is_checked}
        ).returning(TgUserChecklistsPoints)
        result = await self.db_session.execute(on_update_stmt)
        await self.db_session.commit()
        return result.scalar_one_or_none()

    async def get_checklist_points_by_tg_user_id(self, telegram_user_id: uuid.UUID) -> list[TgUserChecklistsPoints]:
        stmt = select(TgUserChecklistsPoints).where(TgUserChecklistsPoints.telegram_user_id == telegram_user_id)
        checklist_points = await self.db_session.execute(stmt)
        return checklist_points.scalars().all()
