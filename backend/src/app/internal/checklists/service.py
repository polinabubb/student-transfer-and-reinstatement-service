import uuid

from fastapi import HTTPException

from src.app.internal.checklists.dto import (
    ChecklistCreateDTO,
    ChecklistDTO,
    ChecklistUpdateDTO,
    TgUserChecklistsPointsDTO,
)
from src.app.internal.checklists.repositories import ChecklistRepository


class ChecklistService:
    def __init__(self, checklist_repo: ChecklistRepository):
        self.checklist_repo = checklist_repo

    async def get_checklists(self) -> list[ChecklistDTO]:
        checklists = await self.checklist_repo.get_all()
        return [ChecklistDTO.model_validate(checklist) for checklist in checklists]

    async def create(self, checklist: ChecklistCreateDTO) -> ChecklistDTO:
        checklist = await self.checklist_repo.create(checklist)
        return ChecklistDTO.model_validate(checklist)

    async def get_checklist(self, checklist_id: int) -> ChecklistDTO:
        checklist = await self.checklist_repo.get(checklist_id)
        if not checklist:
            raise HTTPException(status_code=404, detail="Checklist not found")
        return ChecklistDTO.model_validate(checklist)

    async def get_multi_checklists(self, *, skip: int = 0, limit: int = 100) -> list[ChecklistDTO]:
        checklists = await self.checklist_repo.get_multi(skip=skip, limit=limit)
        return [ChecklistDTO.model_validate(checklist) for checklist in checklists]

    async def update(self, checklist_id: int, checklist_update: ChecklistUpdateDTO) -> ChecklistDTO:
        checklist = await self.checklist_repo.get(checklist_id)
        if not checklist:
            raise HTTPException(status_code=404, detail="Checklist not found")
        for point_update in checklist_update.checklists_points:
            point = await self.checklist_repo.get_point(point_update.id)
            if not point or point.checklist_id != checklist_id:
                raise HTTPException(status_code=404, detail="ChecklistPoint not found")
            await self.checklist_repo.update_checklist_points(point, point_update)
        new_checklist = await self.checklist_repo.update(checklist, checklist_update)
        return ChecklistDTO.model_validate(new_checklist)

    async def delete(self, checklist_id: int) -> None:
        await self.checklist_repo.delete(checklist_id)

    async def set_is_checked_checklist_point(
        self, checklist_point_id: int, telegram_user_id: uuid.UUID, is_checked: bool
    ) -> TgUserChecklistsPointsDTO:
        checklist_point = await self.checklist_repo.get_point(checklist_point_id)
        if not checklist_point:
            raise HTTPException(status_code=404, detail="Checklist point not found")
        new_checklist_point = await self.checklist_repo.set_is_checked_checklist_point(
            checklist_point_id, telegram_user_id, is_checked
        )
        return TgUserChecklistsPointsDTO.model_validate(new_checklist_point)

    async def get_checklist_points_by_tg_user_id(self, telegram_user_id: uuid.UUID) -> list[TgUserChecklistsPointsDTO]:
        checklist_points = await self.checklist_repo.get_checklist_points_by_tg_user_id(telegram_user_id)
        return [TgUserChecklistsPointsDTO.model_validate(checklist_point) for checklist_point in checklist_points]
