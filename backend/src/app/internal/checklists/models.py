import datetime
import enum
import uuid

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.db.base import Base


class ChecklistType(enum.Enum):
    ACADEMIC_VACATION = "ACADEMIC_VACATION"
    RECOVERY = "RECOVERY"
    BUDGET = "BUDGET"
    TRANSFER = "TRANSFER"


class Checklist(Base):
    __tablename__ = "checklists"

    id: Mapped[int] = mapped_column(primary_key=True, unique=True)
    title: Mapped[str]
    description: Mapped[str]
    type: Mapped[ChecklistType]
    updated_at: Mapped[datetime.datetime] = mapped_column(
        nullable=False, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow
    )
    created_at: Mapped[datetime.datetime] = mapped_column(nullable=False, default=datetime.datetime.utcnow)
    checklists_points: Mapped[list["ChecklistPoint"]] = relationship(back_populates="checklist", lazy="joined")


class ChecklistPoint(Base):
    __tablename__ = "checklists_points"

    id: Mapped[int] = mapped_column(primary_key=True, unique=True)
    text: Mapped[str]
    updated_at: Mapped[datetime.datetime] = mapped_column(
        nullable=False, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow
    )
    created_at: Mapped[datetime.datetime] = mapped_column(nullable=False, default=datetime.datetime.utcnow)
    checklist_id: Mapped[int] = mapped_column(ForeignKey("checklists.id", ondelete="CASCADE"), nullable=False)
    checklist: Mapped["Checklist"] = relationship(back_populates="checklists_points")  # noqa: F821


class TgUserChecklistsPoints(Base):
    __tablename__ = "tg_user_checklists_points"

    checklists_point_id: Mapped[int] = mapped_column(
        ForeignKey("checklists_points.id", ondelete="CASCADE"), primary_key=True, nullable=False
    )
    telegram_user_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("telegram_users.id", ondelete="CASCADE"), primary_key=True, nullable=False
    )
    is_checked: Mapped[bool] = mapped_column(default=False, nullable=False)
    updated_at: Mapped[datetime.datetime] = mapped_column(
        nullable=False, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow
    )
    created_at: Mapped[datetime.datetime] = mapped_column(nullable=False, default=datetime.datetime.utcnow)
    # checklists_point: Mapped["ChecklistPoint"] = relationship(back_populates="tg_user_checklists_points")  # noqa: F821
