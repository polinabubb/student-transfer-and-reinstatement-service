from fastapi import HTTPException

from src.app.internal.consts.faculty_data import get_faculties_data
from src.app.internal.faculties.dto import FacultyCourseCreateDTO, FacultyCreateDTO, FacultyDTO, FacultyUpdateDTO
from src.app.internal.faculties.repositories import FacultyRepository
from src.app.internal.parser.parser import get_math_mech_vacant_places


class FacultyService:
    def __init__(self, faculty_repo: FacultyRepository):
        self.faculty_repo = faculty_repo

    async def get_faculties(self) -> list[FacultyDTO]:
        faculties = await self.faculty_repo.get_all()
        return [FacultyDTO.model_validate(faculty) for faculty in faculties]

    async def create(self, faculty: FacultyCreateDTO) -> FacultyDTO:
        faculty = await self.faculty_repo.create(faculty)
        return FacultyDTO.model_validate(faculty)

    async def create_auto(self) -> list[FacultyDTO]:
        faculties = get_faculties_data()
        courses = get_math_mech_vacant_places()
        faculties_to_create = []
        out = []
        await self.faculty_repo.clear_faculties()

        for f in faculties:
            f["courses"] = [FacultyCourseCreateDTO.model_validate(c) for c in courses[f["code"]]]
            faculties_to_create.append(FacultyCreateDTO.model_validate(f))

        for faculty in faculties_to_create:
            out.append(FacultyDTO.model_validate(await self.faculty_repo.create(faculty)))

        return out

    async def get_faculty(self, faculty_id: int) -> FacultyDTO:
        faculty = await self.faculty_repo.get(faculty_id)
        if not faculty:
            raise HTTPException(status_code=404, detail="Faculty not found")
        return FacultyDTO.model_validate(faculty)

    async def get_multi_faculties(self, *, skip: int = 0, limit: int = 100) -> list[FacultyDTO]:
        faculties = await self.faculty_repo.get_multi(skip=skip, limit=limit)
        return [FacultyDTO.model_validate(faculty) for faculty in faculties]

    async def update(self, faculty_id: int, faculty_update: FacultyUpdateDTO) -> FacultyDTO:
        faculty = await self.faculty_repo.get(faculty_id)
        if not faculty:
            raise HTTPException(status_code=404, detail="Faculty not found")
        for course_update in faculty_update.courses:
            course = await self.faculty_repo.get_course(course_update.id)
            if not course or course.faculty_code != faculty.code:
                raise HTTPException(status_code=404, detail="FacultyCourse not found")
            await self.faculty_repo.update_faculty_courses(course, course_update)
        new_faculty = await self.faculty_repo.update(faculty, faculty_update)
        return FacultyDTO.model_validate(new_faculty)

    async def delete(self, faculty_id: int) -> None:
        await self.faculty_repo.delete(faculty_id)
