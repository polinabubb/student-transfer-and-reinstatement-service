import datetime

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.db.base import Base


class Faculty(Base):
    __tablename__ = "faculties"

    id: Mapped[int] = mapped_column(primary_key=True, unique=True)
    code: Mapped[str] = mapped_column(unique=True)
    name: Mapped[str]
    short_name: Mapped[str]
    level: Mapped[str]
    form: Mapped[str]
    link: Mapped[str] = mapped_column(nullable=True)
    educational_program: Mapped[str]
    price: Mapped[int]
    updated_at: Mapped[datetime.datetime] = mapped_column(
        nullable=False, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow
    )
    created_at: Mapped[datetime.datetime] = mapped_column(nullable=False, default=datetime.datetime.utcnow)
    courses: Mapped[list["FacultyCourse"]] = relationship(back_populates="faculty", lazy="joined")


class FacultyCourse(Base):
    __tablename__ = "faculty_courses"

    id: Mapped[int] = mapped_column(primary_key=True, unique=True)
    number: Mapped[int]
    number_budget: Mapped[int]
    number_budget_subject: Mapped[int]
    number_budget_local: Mapped[int]
    number_tuition: Mapped[int]
    updated_at: Mapped[datetime.datetime] = mapped_column(
        nullable=False, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow
    )
    created_at: Mapped[datetime.datetime] = mapped_column(nullable=False, default=datetime.datetime.utcnow)
    faculty_code: Mapped[str] = mapped_column(ForeignKey("faculties.code", ondelete="CASCADE"))
    faculty: Mapped["Faculty"] = relationship(back_populates="courses")
