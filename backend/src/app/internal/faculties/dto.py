import datetime

from pydantic import BaseModel, ConfigDict


class FacultyCourseDTO(BaseModel):
    id: int
    number: int
    number_budget: int
    number_budget_subject: int
    number_budget_local: int
    number_tuition: int
    updated_at: datetime.datetime
    created_at: datetime.datetime
    faculty_code: str

    model_config = ConfigDict(from_attributes=True)


class FacultyDTO(BaseModel):
    id: int
    code: str
    name: str
    short_name: str
    level: str
    form: str
    link: str
    educational_program: str
    price: int
    updated_at: datetime.datetime
    created_at: datetime.datetime
    courses: list[FacultyCourseDTO]

    model_config = ConfigDict(from_attributes=True)


class FacultyOnlyDTO(BaseModel):
    id: int
    code: str
    name: str
    short_name: str
    level: str
    form: str
    link: str
    educational_program: str
    price: int
    updated_at: datetime.datetime
    created_at: datetime.datetime

    model_config = ConfigDict(from_attributes=True)


class FacultyCourseCreateDTO(BaseModel):
    number: int
    number_budget: int
    number_budget_subject: int
    number_budget_local: int
    number_tuition: int
    faculty_code: str


class FacultyCreateDTO(BaseModel):
    code: str
    name: str
    short_name: str
    level: str
    form: str
    link: str
    educational_program: str
    price: int
    courses: list[FacultyCourseCreateDTO]


class FacultyCourseUpdateDTO(BaseModel):
    id: int
    number: int | None = None
    number_budget: int | None = None
    number_budget_subject: int | None = None
    number_budget_local: int | None = None
    number_tuition: int | None = None
    faculty_code: str | None = None


class FacultyUpdateDTO(BaseModel):
    code: str | None = None
    name: str | None = None
    short_name: str
    level: str | None = None
    form: str | None = None
    link: str | None = None
    educational_program: str | None = None
    price: int | None
    courses: list[FacultyCourseUpdateDTO] | None = []
