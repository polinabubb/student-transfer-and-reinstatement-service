from pydantic import BaseModel


class FacultyCourseCreateRequest(BaseModel):
    number: int
    number_budget: int
    number_budget_subject: int
    number_budget_local: int
    number_tuition: int


class FacultyCreateRequest(BaseModel):
    code: str
    name: str
    short_name: str
    level: str
    form: str
    link: str
    educational_program: str
    price: int
    courses: list[FacultyCourseCreateRequest]


class FacultyCourseUpdateRequest(BaseModel):
    id: int
    number: int | None = None
    number_budget: int | None = None
    number_budget_subject: int | None = None
    number_budget_local: int | None = None
    number_tuition: int | None = None


class FacultyUpdateRequest(BaseModel):
    code: str | None = None
    name: str | None = None
    short_name: str | None = None
    level: str | None = None
    form: str | None = None
    link: str | None = None
    educational_program: str | None = None
    price: int | None = None
    courses: list[FacultyCourseUpdateRequest] | None = []
