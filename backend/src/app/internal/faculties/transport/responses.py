import datetime

from pydantic import BaseModel


class FacultyCourseResponse(BaseModel):
    id: int
    number: int
    number_budget: int
    number_budget_subject: int
    number_budget_local: int
    number_tuition: int
    updated_at: datetime.datetime
    created_at: datetime.datetime
    faculty_code: str


class FacultyResponse(BaseModel):
    id: int
    code: str
    name: str
    short_name: str
    level: str
    form: str
    link: str
    educational_program: str
    price: int
    updated_at: datetime.datetime
    created_at: datetime.datetime
    courses: list[FacultyCourseResponse]
