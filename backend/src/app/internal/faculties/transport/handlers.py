from typing import Annotated

from fastapi import APIRouter, Depends

from src.app.internal.core.auth.middlewares.jwt.service import get_current_user
from src.app.internal.faculties.service import FacultyService
from src.app.internal.faculties.transport.di import get_faculty_service
from src.app.internal.faculties.transport.requests import FacultyCreateRequest, FacultyUpdateRequest
from src.app.internal.faculties.transport.responses import FacultyResponse
from src.app.internal.users.models import User

faculties_router = APIRouter(prefix="/faculties", tags=["faculties"])


@faculties_router.get(
    path="",
    response_model=list[FacultyResponse],
    status_code=200,
)
async def get_multi_faculties(
    faculty_service: Annotated[FacultyService, Depends(get_faculty_service)],
    skip: int = 0,
    limit: int = 100,
) -> list[FacultyResponse]:
    return await faculty_service.get_multi_faculties(skip=skip, limit=limit)


@faculties_router.get(
    path="/{faculty_id}",
    response_model=FacultyResponse,
    status_code=200,
)
async def get_faculty(
    faculty_id: int,
    faculty_service: Annotated[FacultyService, Depends(get_faculty_service)],
) -> FacultyResponse:
    return await faculty_service.get_faculty(faculty_id)


@faculties_router.post(
    path="",
    response_model=FacultyResponse,
    status_code=201,
)
async def create_faculty(
    faculty: FacultyCreateRequest,
    current_user: Annotated[User, Depends(get_current_user)],
    faculty_service: Annotated[FacultyService, Depends(get_faculty_service)],
) -> FacultyResponse:
    return await faculty_service.create(faculty)


@faculties_router.patch(
    path="/{faculty_id}",
    response_model=FacultyResponse,
    status_code=200,
)
async def update_faculty(
    faculty_id: int,
    faculty: FacultyUpdateRequest,
    current_user: Annotated[User, Depends(get_current_user)],
    faculty_service: Annotated[FacultyService, Depends(get_faculty_service)],
) -> FacultyResponse:
    return await faculty_service.update(faculty_id, faculty)


@faculties_router.delete(
    path="/{faculty_id}",
    status_code=204,
)
async def delete_faculty(
    faculty_id: int,
    current_user: Annotated[User, Depends(get_current_user)],
    faculty_service: Annotated[FacultyService, Depends(get_faculty_service)],
):
    await faculty_service.delete(faculty_id)


@faculties_router.post(
    path="/create_auto/",
    response_model=list[FacultyResponse],
    status_code=200,
)
async def create_auto(
    faculty_service: Annotated[FacultyService, Depends(get_faculty_service)],
) -> list[FacultyResponse]:
    return await faculty_service.create_auto()
