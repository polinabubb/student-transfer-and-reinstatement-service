from typing import Annotated

from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from src.app.internal.faculties.repositories import FacultyRepository
from src.app.internal.faculties.service import FacultyService
from src.db.di import get_db


async def get_faculty_service(db_session: Annotated[AsyncSession, Depends(get_db)]) -> FacultyService:
    faculty_repo = FacultyRepository(db_session=db_session)
    return FacultyService(faculty_repo=faculty_repo)
