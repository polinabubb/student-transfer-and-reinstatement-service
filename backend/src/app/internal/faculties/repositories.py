import uuid
from typing import Any, Union

from src.app.internal.faculties.dto import FacultyCourseUpdateDTO, FacultyCreateDTO, FacultyUpdateDTO
from src.app.internal.faculties.models import Faculty, FacultyCourse
from src.db.base_repository import BaseRepository


class FacultyRepository(BaseRepository[Faculty, FacultyCreateDTO, FacultyUpdateDTO]):
    model = Faculty

    async def create(self, faculty: FacultyCreateDTO) -> Faculty:
        new_faculty = Faculty(
            code=faculty.code,
            name=faculty.name,
            short_name=faculty.short_name,
            level=faculty.level,
            form=faculty.form,
            link=faculty.link,
            price=faculty.price,
            educational_program=faculty.educational_program,
        )
        for course in faculty.courses:
            faculty_course = FacultyCourse(
                number=course.number,
                number_budget=course.number_budget,
                number_budget_subject=course.number_budget_subject,
                number_budget_local=course.number_budget_local,
                number_tuition=course.number_tuition,
            )
            new_faculty.courses.append(faculty_course)
        self.db_session.add(new_faculty)
        await self.db_session.commit()
        await self.db_session.refresh(new_faculty)
        return new_faculty

    async def update(self, model: Faculty, update_model: Union[FacultyUpdateDTO, dict[str, Any]]) -> Faculty | None:
        if isinstance(update_model, dict):
            update_data = update_model
        else:
            update_data = update_model.model_dump(exclude_unset=True)
        if update_data.get("courses", None):
            del update_data["courses"]
        return await super().update(model, update_data)

    async def get_course(self, id: int | str | uuid.UUID) -> FacultyCourse | None:
        db_model = await self.db_session.get(FacultyCourse, id)
        return db_model

    async def update_faculty_courses(
        self, model: FacultyCourse, update_model: Union[FacultyCourseUpdateDTO, dict[str, Any]]
    ) -> FacultyCourse | None:
        return await super().update(model, update_model)

    async def clear_faculties(self) -> FacultyCourse | None:
        return await super().clear("faculties")
