from typing import Annotated

from fastapi import APIRouter, Depends

from src.app.internal.core.auth.middlewares.jwt.service import get_current_user
from src.app.internal.users.models import User
from src.app.internal.users.service import UserService
from src.app.internal.users.transport.di import get_user_service
from src.app.internal.users.transport.responses import UserProfileResponse

users_router = APIRouter(prefix="/users", tags=["users"])


@users_router.get(
    path="/me",
    response_model=UserProfileResponse,
    status_code=200,
)
async def get_me(
    current_user: Annotated[User, Depends(get_current_user)],
    user_service: Annotated[UserService, Depends(get_user_service)],
) -> UserProfileResponse:
    return user_service.get_me(current_user)


@users_router.get(
    path="/{user_id}",
    response_model=UserProfileResponse,
    status_code=200,
)
async def get_user(
    user_id: str,
    user_service: Annotated[UserService, Depends(get_user_service)],
) -> UserProfileResponse:
    return await user_service.get_user(user_id)


@users_router.get(
    path="",
    response_model=list[UserProfileResponse],
    status_code=200,
)
async def get_users(
    users_service: Annotated[UserService, Depends(get_user_service)],
) -> list[UserProfileResponse]:
    return await users_service.get_users()
