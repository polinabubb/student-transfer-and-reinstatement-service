from uuid import UUID

from pydantic import BaseModel, ConfigDict


class UserProfileDTO(BaseModel):
    id: UUID
    email: str

    model_config = ConfigDict(from_attributes=True)


class UserCreateDTO(BaseModel):
    email: str
    hashed_password: str


class UserUpdateDTO(BaseModel):
    email: str | None = None
    password: str | None = None


class UserDTO(BaseModel):
    email: str
    password: str
