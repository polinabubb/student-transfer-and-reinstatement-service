from sqlalchemy import select

from src.app.internal.users.dto import UserCreateDTO, UserUpdateDTO
from src.app.internal.users.models import User
from src.db.base_repository import BaseRepository


class UserRepository(BaseRepository[User, UserCreateDTO, UserUpdateDTO]):
    model = User

    async def get_by_email(self, email: str) -> User:
        stmt = select(User).where(User.email == email)
        user = await self.db_session.execute(stmt)
        return user.scalar_one_or_none()
