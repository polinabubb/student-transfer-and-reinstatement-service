from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.orm import Mapped, mapped_column

from src.db.base import Base


class ChecklistNew(Base):
    __tablename__ = "checklists_new"

    id: Mapped[int] = mapped_column(primary_key=True, unique=True)
    json_data: Mapped[dict] = mapped_column(JSON, nullable=True)
