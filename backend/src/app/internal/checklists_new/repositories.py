import uuid
from typing import Any, Union

from sqlalchemy import delete, select

from src.app.internal.checklists_new.dto import ChecklistNewCreateDTO, ChecklistNewUpdateDTO
from src.app.internal.checklists_new.models import ChecklistNew
from src.db.base_repository import BaseRepository


class ChecklistNewRepository(BaseRepository[ChecklistNew, ChecklistNewCreateDTO, ChecklistNewUpdateDTO]):
    model = ChecklistNew

    async def create(self, model_in: ChecklistNewCreateDTO) -> ChecklistNew:
        db_model = ChecklistNew(json_data=model_in.model_dump())
        self.db_session.add(db_model)
        await self.db_session.commit()
        await self.db_session.refresh(db_model)
        return db_model

    async def get(self, id: int | str | uuid.UUID) -> ChecklistNew | None:
        db_model = await self.db_session.get(ChecklistNew, id)
        return db_model

    async def get_multi(self, *, skip: int = 0, limit: int = 100) -> list[ChecklistNew]:
        stmt = select(ChecklistNew).offset(skip).limit(limit)
        models = await self.db_session.execute(stmt)
        return models.unique().scalars().all()

    async def update(
        self, model: ChecklistNew, update_model: Union[ChecklistNewUpdateDTO, dict[str, Any]]
    ) -> ChecklistNew | None:
        model.json_data = update_model.model_dump()
        self.db_session.add(model)
        await self.db_session.commit()
        await self.db_session.refresh(model)
        return model

    async def delete(self, id: int | str | uuid.UUID) -> None:
        stmt = delete(ChecklistNew).where(self.model.id == id)
        await self.db_session.execute(stmt)
        await self.db_session.commit()
