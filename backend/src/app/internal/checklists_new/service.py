from fastapi import HTTPException

from src.app.internal.checklists_new.dto import ChecklistNewCreateDTO, ChecklistNewDTO, ChecklistNewUpdateDTO
from src.app.internal.checklists_new.repositories import ChecklistNewRepository


class ChecklistNewService:
    def __init__(self, checklist_repo: ChecklistNewRepository):
        self.checklist_repo = checklist_repo

    async def create(self, checklist: ChecklistNewCreateDTO) -> ChecklistNewDTO:
        checklist = await self.checklist_repo.create(checklist)
        return ChecklistNewDTO(id=checklist.id, **checklist.json_data)

    async def get_checklist(self, checklist_id: int) -> ChecklistNewDTO:
        checklist = await self.checklist_repo.get(checklist_id)
        if not checklist:
            raise HTTPException(status_code=404, detail="Checklist not found")
        return ChecklistNewDTO(id=checklist.id, **checklist.json_data)

    async def get_multi_checklists(self, *, skip: int = 0, limit: int = 100) -> list[ChecklistNewDTO]:
        checklists = await self.checklist_repo.get_multi(skip=skip, limit=limit)
        return [ChecklistNewDTO(id=checklist.id, **checklist.json_data) for checklist in checklists]

    async def update(self, checklist_id: int, checklist_update: ChecklistNewUpdateDTO) -> ChecklistNewDTO:
        checklist = await self.checklist_repo.get(checklist_id)
        if not checklist:
            raise HTTPException(status_code=404, detail="Checklist not found")
        new_checklist = await self.checklist_repo.update(checklist, checklist_update)
        return ChecklistNewDTO(id=new_checklist.id, **new_checklist.json_data)

    async def delete(self, checklist_id: int) -> None:
        await self.checklist_repo.delete(checklist_id)
