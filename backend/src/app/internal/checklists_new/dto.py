from typing import Optional

from pydantic import BaseModel


class StepPart(BaseModel):
    text: str
    isLink: bool
    link: Optional[str]
    isLineBreak: bool


class AdditionalStep(BaseModel):
    text: str
    isLineBreak: bool


class Step(BaseModel):
    isChecked: bool = False
    stepParts: list[StepPart]
    additionToStep: Optional[list[AdditionalStep]]


class ChecklistNewDTO(BaseModel):
    id: int
    name: str
    steps: list[Step]
    requirements: list[str]
    finalRecommendation: Optional[Step]
    additional_info: Optional[list[dict]]


class ChecklistNewCreateDTO(BaseModel):
    name: str
    steps: list[Step]
    requirements: list[str]
    finalRecommendation: Optional[Step]
    additional_info: Optional[list[dict]]


class ChecklistNewUpdateDTO(BaseModel):
    name: str | None = None
    steps: list[Step] | None = None
    requirements: list[str] | None = None
    finalRecommendation: Optional[Step] = None
    additional_info: Optional[list[dict]] = None
