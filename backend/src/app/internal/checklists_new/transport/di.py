from typing import Annotated

from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from src.app.internal.checklists_new.repositories import ChecklistNewRepository
from src.app.internal.checklists_new.service import ChecklistNewService
from src.db.di import get_db


async def get_checklist_new_service(db_session: Annotated[AsyncSession, Depends(get_db)]) -> ChecklistNewService:
    checklist_repo = ChecklistNewRepository(db_session=db_session)
    return ChecklistNewService(checklist_repo=checklist_repo)
