from typing import Annotated

from fastapi import APIRouter, Depends

from src.app.internal.checklists_new.dto import ChecklistNewCreateDTO, ChecklistNewDTO, ChecklistNewUpdateDTO
from src.app.internal.checklists_new.service import ChecklistNewService
from src.app.internal.checklists_new.transport.di import get_checklist_new_service
from src.app.internal.core.auth.middlewares.jwt.service import get_current_user
from src.app.internal.users.models import User

checklists_new_router = APIRouter(prefix="/checklists_new", tags=["checklists_new"])


@checklists_new_router.get(
    path="",
    response_model=list[ChecklistNewDTO],
    status_code=200,
)
async def get_multi_checklists(
    checklist_service: Annotated[ChecklistNewService, Depends(get_checklist_new_service)],
    skip: int = 0,
    limit: int = 100,
) -> list[ChecklistNewDTO]:
    return await checklist_service.get_multi_checklists(skip=skip, limit=limit)


@checklists_new_router.get(
    path="/{checklist_id}",
    response_model=ChecklistNewDTO,
    status_code=200,
)
async def get_checklist(
    checklist_id: int,
    checklist_service: Annotated[ChecklistNewService, Depends(get_checklist_new_service)],
) -> ChecklistNewDTO:
    return await checklist_service.get_checklist(checklist_id)


@checklists_new_router.post(
    path="",
    response_model=ChecklistNewDTO,
    status_code=201,
)
async def create_checklist(
    checklist: ChecklistNewCreateDTO,
    current_user: Annotated[User, Depends(get_current_user)],
    checklist_service: Annotated[ChecklistNewService, Depends(get_checklist_new_service)],
) -> ChecklistNewDTO:
    return await checklist_service.create(checklist)


@checklists_new_router.patch(
    path="/{checklist_id}",
    response_model=ChecklistNewDTO,
    status_code=200,
)
async def update_checklist(
    checklist_id: int,
    checklist: ChecklistNewUpdateDTO,
    current_user: Annotated[User, Depends(get_current_user)],
    checklist_service: Annotated[ChecklistNewService, Depends(get_checklist_new_service)],
) -> ChecklistNewDTO:
    return await checklist_service.update(checklist_id, checklist)


@checklists_new_router.delete(
    path="/{checklist_id}",
    status_code=204,
)
async def delete_checklist(
    checklist_id: int,
    current_user: Annotated[User, Depends(get_current_user)],
    checklist_service: Annotated[ChecklistNewService, Depends(get_checklist_new_service)],
):
    await checklist_service.delete(checklist_id)
