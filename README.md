# Vobladogs

[![pipeline](https://gitlab.com/vobladogs/backend/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/vobladogs/backend/badges/main/coverage.svg)
[![coverage](https://gitlab.com/vobladogs/backend/badges/main/coverage.svg)](https://gitlab.com/vobladogs/backend/badges/main/coverage.svg)
[![total lines](https://tokei.rs/b1/gitlab/vobladogs/backend)](https://gitlab.com/vobladogs/backend)
[![Hits-of-Code](https://hitsofcode.com/gitlab/vobladogs/backend?branch=main)](https://hitsofcode.com/gitlab/vobladogs/backend/view?branch=main)


## Getting started

```bash
cp backend/.env.example backend/.env
cp telegram/.env.example telegram/.env
```
- Fill TELEGRAM_TOKEN at telegram/.env with your token

```bash
make
```

## Start backend and database
- Change `POSTGRES_HOST` at `.env` to `database`
```bash
cd backend
make
```

## Start frontend
- Run in dev mode
```bash
cd frontend
npm run dev
```

## Start telegram
```bash
cd telegram
make
```

## Technologies used

- PostgreSQL
- Docker
- Docker Compose
- Nginx

### Backend

- Python
- FastAPI
- SQLAlchemy
- pytest
- asyncpg

### Frontend

- Typescript
- React

### Telegram

- Python
- aiogram
- aiohttp

## Deploy

- Yandex Cloud
